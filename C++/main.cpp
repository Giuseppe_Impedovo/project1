#include "test_cutpolygon.hpp"
#include "test_intersector.hpp"
#include "test_polygon.hpp"


#include <gtest/gtest.h>



int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
