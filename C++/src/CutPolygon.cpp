﻿#include "CutPolygon.hpp"

namespace CutPolygonNamespace
{
    void CutPolygon::SavePolygon(const vector<Point> &points, const vector<int> &vertices)
    {
        int numPoints = points.size();
        for (int i = 0; i < numPoints; i++)
            newPoints.push_back(points[i]);

        int i = 0;
        for (list<Point>::iterator it = newPoints.begin(); it != newPoints.end(); it++)
        {
            it->numVert = vertices[i];
            i++;
        }

        polygonEdges.reserve(numPoints);
        for (int i = 0; i < numPoints; i++)
        {
            if (i == numPoints - 1 && numPoints > 2)
            {
                Segment tmpSegment = Segment(points[i], points[0]);
                tmpSegment.From.passed = false;
                tmpSegment.From.numVert = i;
                polygonEdges.push_back(tmpSegment);
            }
            else if (i != numPoints - 1)
            {
                Segment tmpSegment = Segment(points[i], points[i + 1]);
                tmpSegment.From.passed = false;
                tmpSegment.From.numVert = i;
                polygonEdges.push_back(tmpSegment);
            }
        }
    }

    void CutPolygon::Cut(const vector<Point> &points, const vector<int> &vertices, const Segment &segment)
    {
        SavePolygon(points, vertices);
        int numPoints = newPoints.size();
        for(unsigned int i = 0; i < polygonEdges.size(); i++)
        {
            polygonEdges[i].intersection = _intersecatore.ComputeIntersection(segment, polygonEdges[i], newPoints, i, intersectionPointsList, numPoints);
        }
        // controlliamo se i punti del segmento che taglia siano dentro al poligono
        int flagFrom = 0;
        int flagTo = 0;
        for (list<Point>::iterator it = intersectionPointsList.begin(); it != intersectionPointsList.end() && (flagFrom == 0 || flagTo == 0); it++)
        {
            if(it->paramCoord < toleranceIntersection && flagFrom == 0)
            {
                flagFrom = 1;
            }
            if(it->paramCoord > 1 - toleranceIntersection && flagTo == 0)
            {
                flagTo = 1;
            }
        }
        if(flagFrom == 1)
        {
            Point segFromPoint = Point(segment.From._x, segment.From._y);
            segFromPoint.paramCoord = 0.0;
            segFromPoint.appSegment = -1;
            segFromPoint.numVert = numPoints;
            numPoints++;
            newPoints.push_back(segFromPoint);
            intersectionPointsList.push_back(segFromPoint);
        }
        if(flagTo == 1)
        {
            Point segToPoint = Point(segment.To._x, segment.To._y);
            segToPoint.paramCoord = 1;
            segToPoint.appSegment = -1;
            segToPoint.numVert = numPoints;
            numPoints++;
            newPoints.push_back(segToPoint);
            intersectionPointsList.push_back(segToPoint);
        }

        // trasferiamo le intersezioni dalla lista al vettore, ordinandole tramita coordinata curvilinea
        intersectionPointsVec.reserve(intersectionPointsList.size());
        int i = 0;
        int flag3;
        for (list<Point>::iterator it = intersectionPointsList.begin(); it != intersectionPointsList.end(); it++)
        {
            flag3 = 1;
            intersectionPointsVec.push_back(*it);
            for(int j = i; j > 0  && flag3 == 1; j--)
            {
                if(intersectionPointsVec[j].paramCoord < intersectionPointsVec[j - 1].paramCoord)
                {
                    Point tmpPoint = intersectionPointsVec[j];
                    intersectionPointsVec[j] = intersectionPointsVec[j - 1];
                    intersectionPointsVec[j - 1] = tmpPoint;
                }
                else
                {
                    flag3 = 0;
                }

            }
            i++;
        }

        // utile per ordinare correttamente due punti coincidenti nel vettore
        for(unsigned int i = 1; i < intersectionPointsVec.size(); i++)
        {
            if(intersectionPointsVec[i].numVert == intersectionPointsVec[i - 1].numVert)
            {
                double controllo;
                int indice = intersectionPointsVec[i].appSegment - 1;
                controllo = (segment.To._x - segment.From._x)*(polygonEdges[indice].From._y - segment.From._y) - (segment.To._y - segment.From._y)*(polygonEdges[indice].From._x - segment.From._x);
                if(controllo > toleranceIntersection)
                {
                    Point tmpPoint = intersectionPointsVec[i];
                    intersectionPointsVec[i] = intersectionPointsVec[i - 1];
                    intersectionPointsVec[i - 1] = tmpPoint;
                }

            }
        }

        // accoppiare le intersezioni
        int flag4 = 0;
        int friendship = 0;
        for(unsigned int i = 0; i < intersectionPointsVec.size(); i++)
        {
            if(intersectionPointsVec[i].appSegment != -1)
            {
                if(i == 0 && intersectionPointsVec[i].numVert == intersectionPointsVec[i + 1].numVert)
                {
                    intersectionPointsVec[i].friendship = friendship;
                    friendship++;
                }
                else
                {
                    intersectionPointsVec[i].friendship = friendship;
                    if(flag4 < 1)
                        flag4++;
                    else
                    {
                        flag4--;
                        friendship++;
                    }
                }
            }
            else
                intersectionPointsVec[i].friendship = -1;
        }

        //creazione dei poligoni
        Intersector intersecatore;
        CutPolygon cutter(intersecatore);
        Polygon tmpPolygon(cutter);
        int flag5 = 0;
        int flagFine = 0;
        for(int s = 0; s < (int)polygonEdges.size() && flagFine != (int)points.size() ; s++)
        {
            if(polygonEdges[s].From.passed == false)
            {
                polygonEdges[s].From.passed = true;
                flagFine++;
                tmpPolygon.newPolygonVertices.push_back(polygonEdges[s].From.numVert);
                tmpPolygon.newPolygonPoints.push_back(polygonEdges[s].From);
                if(polygonEdges[s].intersection == true)
                {
                    flag5 = 0;
                    for(unsigned int i = 0; i < intersectionPointsVec.size() && flag5 == 0; i++)
                    {
                        if(s == intersectionPointsVec[i].appSegment)
                        {
                            flag5 = 1;
                            Point arrival = Point(0.0,0.0);
                            if(intersectionPointsVec[i].enumTypeIntersection != 0)
                            {
                                tmpPolygon.newPolygonVertices.push_back(intersectionPointsVec[i].numVert);
                                tmpPolygon.newPolygonPoints.push_back(intersectionPointsVec[i]);
                            }
                            if(i + 1 < intersectionPointsVec.size() && intersectionPointsVec[i + 1].friendship == intersectionPointsVec[i].friendship)
                            {
                                arrival = intersectionPointsVec[i + 1];
                                s = intersectionPointsVec[i + 1].appSegment;
                            }
                            else if(i + 1 < intersectionPointsVec.size() && intersectionPointsVec[i + 1].friendship == -1 && intersectionPointsVec[i + 2].friendship == intersectionPointsVec[i].friendship)
                            {
                                 tmpPolygon.newPolygonVertices.push_back(intersectionPointsVec[i + 1].numVert);
                                 tmpPolygon.newPolygonPoints.push_back(intersectionPointsVec[i + 1]);
                                 arrival = intersectionPointsVec[i + 2];
                                 s = intersectionPointsVec[i + 2].appSegment;
                            }
                            else if(i + 1 < intersectionPointsVec.size() && intersectionPointsVec[i + 1].friendship == -1 && intersectionPointsVec[i + 2].friendship == -1 && intersectionPointsVec[i + 3].friendship == intersectionPointsVec[i].friendship)
                            {
                                 tmpPolygon.newPolygonVertices.push_back(intersectionPointsVec[i + 1].numVert);
                                 tmpPolygon.newPolygonVertices.push_back(intersectionPointsVec[i + 2].numVert);
                                 tmpPolygon.newPolygonPoints.push_back(intersectionPointsVec[i + 1]);
                                 tmpPolygon.newPolygonPoints.push_back(intersectionPointsVec[i + 2]);
                                 arrival = intersectionPointsVec[i + 3];
                                 s = intersectionPointsVec[i + 3].appSegment;
                            }
                            else if(intersectionPointsVec[i - 1].friendship == intersectionPointsVec[i].friendship)
                            {
                                 arrival = intersectionPointsVec[i - 1];
                                 s = intersectionPointsVec[i - 1].appSegment;
                            }
                            else if(intersectionPointsVec[i - 1].friendship == -1 && intersectionPointsVec[i - 2].friendship == intersectionPointsVec[i].friendship)
                            {
                                 tmpPolygon.newPolygonVertices.push_back(intersectionPointsVec[i - 1].numVert);
                                 tmpPolygon.newPolygonPoints.push_back(intersectionPointsVec[i - 1]);
                                 arrival = intersectionPointsVec[i - 2];
                                 s = intersectionPointsVec[i - 2].appSegment;
                            }
                            else if(intersectionPointsVec[i - 1].friendship == -1 && intersectionPointsVec[i - 2].friendship == -1 && intersectionPointsVec[i - 3].friendship == intersectionPointsVec[i].friendship)
                            {
                                 tmpPolygon.newPolygonVertices.push_back(intersectionPointsVec[i - 1].numVert);
                                 tmpPolygon.newPolygonVertices.push_back(intersectionPointsVec[i - 2].numVert);
                                 tmpPolygon.newPolygonPoints.push_back(intersectionPointsVec[i - 1]);
                                 tmpPolygon.newPolygonPoints.push_back(intersectionPointsVec[i - 2]);
                                 arrival = intersectionPointsVec[i - 3];
                                 s = intersectionPointsVec[i - 3].appSegment;
                            }

                            if(arrival.enumTypeIntersection != 2 && s == arrival.appSegment)
                            {
                                tmpPolygon.newPolygonVertices.push_back(arrival.numVert);
                                tmpPolygon.newPolygonPoints.push_back(arrival);
                            }
                        }

                    }
                }
                if(s == (int)polygonEdges.size() - 1 && flagFine != (int)points.size())
                    s = -1;
            }
            else if(polygonEdges[s].From.passed == true && tmpPolygon.newPolygonVertices.size() > 2)
            {
                cuttedPolygon.push_back(tmpPolygon);
                tmpPolygon.newPolygonVertices.clear();
                tmpPolygon.newPolygonPoints.clear();
                s = 0;
            }
            if(flagFine == (int)points.size() &&  tmpPolygon.newPolygonVertices.size() > 2)
                cuttedPolygon.push_back(tmpPolygon);
        }

        for (list<Polygon>::iterator it = cuttedPolygon.begin(); it != cuttedPolygon.end(); it++)
        {
            it->pPoints.reserve(it->newPolygonVertices.size());
            it->verticesVec.reserve(it->newPolygonVertices.size());
            for(list<Point>::iterator it2 = it->newPolygonPoints.begin(); it2 != it->newPolygonPoints.end(); it2++)
                it->pPoints.push_back(*it2);
            for(list<int>::iterator it3 = it->newPolygonVertices.begin(); it3 != it->newPolygonVertices.end(); it3++)
                it->verticesVec.push_back(*it3);
        }


    }






}
