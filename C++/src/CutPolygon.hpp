#ifndef CUTPOLYGON_H
#define CUTPOLYGON_H

#include "baseClass.hpp"
#include "Polygon.hpp"
#include "Eigen"
#include <iostream>
#include <list>
#include <vector>

using namespace std;
using namespace Eigen;
using namespace BaseClassNamespace;
using namespace PolygonNamespace;

namespace CutPolygonNamespace
{
    class CutPolygon : public ICutPolygon
    {
    public:
        list <Point> newPoints;
        list <Point> intersectionPointsList;
        vector <Point> intersectionPointsVec;
        vector <Segment> polygonEdges;
        IIntersector& _intersecatore;
        list <Polygon> cuttedPolygon;
    private:
        double toleranceIntersection = 1.0E-7;

    public:
        CutPolygon(IIntersector& intersecatore): _intersecatore(intersecatore){}
        void SavePolygon(const vector<Point>& points, const vector<int>& vertices);
        void Cut(const vector<Point>& points, const vector<int>& vertices, const Segment& segment);

    };
}

#endif
