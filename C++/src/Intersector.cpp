#include "Intersector.hpp"

namespace IntersectorNamespace
{
    void Intersector::SaveSegments(const Segment &cutterSegment, const Segment &polygonEdge)
    {
        Vector2d originCutterVector;
        Vector2d endCutterVector;
        Vector2d originPolygonVector;
        Vector2d endPolygonVector;
        originCutterVector[0] = cutterSegment.From._x;
        originCutterVector[1] = cutterSegment.From._y;
        endCutterVector[0] = cutterSegment.To._x;
        endCutterVector[1] = cutterSegment.To._y;
        originPolygonVector[0] = polygonEdge.From._x;
        originPolygonVector[1] = polygonEdge.From._y;
        endPolygonVector[0] = polygonEdge.To._x;
        endPolygonVector[1] = polygonEdge.To._y;
        SetCutterSegment(originCutterVector, endCutterVector);
        SetPolygonEdge(originPolygonVector, endPolygonVector);
    }

    Intersector::Intersector()
    {
        type = Intersector::NoIntersection;
    }

    bool Intersector::ComputeIntersection(const Segment &cutterSegment, const Segment &polygonEdge, list<Point> &newPoints, unsigned int &appEdge, list<Point> &intersectionPointsList, int &numPoints)
    {
        SaveSegments(cutterSegment, polygonEdge);
        double determinant = matrixTangentVector.determinant();
        type = NoIntersection;
        bool intersection = false;
        double check = toleranceParallelism * toleranceParallelism * matrixTangentVector.col(0).squaredNorm() *  matrixTangentVector.col(1).squaredNorm();

        if(determinant * determinant>= check)  // check per controllare se sono paralleli o no
        {
            Matrix2d solverMatrix; // inversa della matrixTangentVector usata per trovare il vettore delle coordinate parametriche
            solverMatrix << matrixTangentVector(1,1), -matrixTangentVector(0,1), -matrixTangentVector(1,0), matrixTangentVector(0,0);
            resultParametricCoordinates = (solverMatrix * rightHandSide) / determinant; // non avendo diviso per il determinante l'inversa, dividiamo la soluzione in questa linea
            if (resultParametricCoordinates(1) > -toleranceIntersection  && resultParametricCoordinates(1) < 1.0 + toleranceIntersection)
            {
                type = IntersectionOnLine;
                intersection = true;
                if (resultParametricCoordinates(0) > -toleranceIntersection  && resultParametricCoordinates(0) < 1.0 + toleranceIntersection)
                type = IntersectionOnSegment;
            }
        }
        else // qui entra se sono paralleli
        {
            double determinant2 = fabs(matrixTangentVector(0,0) * rightHandSide.y() - rightHandSide.x() * matrixTangentVector(1,0));
            double check2 = toleranceParallelism * toleranceParallelism * matrixTangentVector.col(0).squaredNorm() * rightHandSide.squaredNorm();
            if( determinant2 * determinant2 <= check2 ) // nuovo controllo per stabilire se sono sulla stessa retta
            {
                double tempNorm = 1.0 / matrixTangentVector.col(0).squaredNorm();
                resultParametricCoordinates(0) = matrixTangentVector.col(0).dot(rightHandSide) * tempNorm;
                resultParametricCoordinates(1) = resultParametricCoordinates(0) - matrixTangentVector.col(0).dot(matrixTangentVector.col(1)) * tempNorm;
                intersection = true;
                type = IntersectionParallelOnLine;
                if(resultParametricCoordinates(1) < resultParametricCoordinates(0))
                {
                    double tmp = resultParametricCoordinates(0);
                    resultParametricCoordinates(0) = resultParametricCoordinates(1);
                    resultParametricCoordinates(1) = tmp;
                }

                if((resultParametricCoordinates(0) > -toleranceIntersection && resultParametricCoordinates(0) < 1.0 + toleranceIntersection) ||
                   (resultParametricCoordinates(1) > -toleranceIntersection && resultParametricCoordinates(1) < 1.0 + toleranceIntersection))
                    type = IntersectionParallelOnSegment;
                else
                {
                    if((resultParametricCoordinates(0) < toleranceIntersection && resultParametricCoordinates(1) - 1.0 > -toleranceIntersection))
                        type = IntersectionParallelOnSegment;
                }
            }
        }

        // distinguendo tra begin, end e inner aggiungiamo i risultati ottenuti negli attributi di CutPolygon passati in input
        if(type != IntersectionParallelOnLine && type != IntersectionParallelOnSegment)
        {
            if(type != NoIntersection)
            {
                Point tmpPoint = Point((rightHandSide[0] + originFirstSegment[0]) + resultParametricCoordinates(1) * (-matrixTangentVector(0,1)), (rightHandSide[1] + originFirstSegment[1]) + resultParametricCoordinates(1) * (-matrixTangentVector(1,1)));
                tmpPoint.appSegment =appEdge;
                tmpPoint.paramCoord = resultParametricCoordinates(0);
                if(resultParametricCoordinates(1) > -toleranceIntersection && resultParametricCoordinates(1) < toleranceIntersection)
                {
                    tmpPoint.enumTypeIntersection = 0;
                    tmpPoint.numVert = polygonEdge.From.numVert;
                    intersectionPointsList.push_back(tmpPoint);

                }
                else if(resultParametricCoordinates(1) > 1.0 - toleranceIntersection && resultParametricCoordinates(1) < 1.0 + toleranceIntersection)
                {
                    tmpPoint.numVert = polygonEdge.To.numVert;
                    tmpPoint.enumTypeIntersection = 2;
                    intersectionPointsList.push_back(tmpPoint);

                }
                else
                {
                    tmpPoint.numVert = numPoints;
                    tmpPoint.enumTypeIntersection = 1;
                    newPoints.push_back(tmpPoint);
                    intersectionPointsList.push_back(tmpPoint);
                    numPoints++;
                }
            }
        }
        return intersection;
    }
}
