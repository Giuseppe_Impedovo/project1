#ifndef INTERSECTOR_H
#define INTERSECTOR_H

#include "baseClass.hpp"
#include "Eigen"

using namespace std;
using namespace Eigen;
using namespace BaseClassNamespace;

namespace IntersectorNamespace
{
class Intersector : public IIntersector
{
    public:
        enum Type
        {
            NoIntersection = 0,
            IntersectionOnLine = 1,
            IntersectionOnSegment = 2,
            IntersectionParallelOnLine = 3,
            IntersectionParallelOnSegment = 4,
        };

        Type type;

        Vector2d resultParametricCoordinates;
        Vector2d originFirstSegment;
        Vector2d rightHandSide;
        Matrix2d matrixTangentVector;
    private:
        double toleranceParallelism = 1.0E-7;
        double toleranceIntersection = 1.0E-7;

    public:
        Intersector();
        void SaveSegments(const Segment& cutterSegment, const Segment& polygonEdge);
        void SetCutterSegment(const Vector2d& origin, const Vector2d& end)
        {
            matrixTangentVector.col(0) = end - origin;
            originFirstSegment = origin;
        }
        void SetPolygonEdge(const Vector2d& origin, const Vector2d& end)
        {
            matrixTangentVector.col(1) = origin - end;
            rightHandSide = origin - originFirstSegment;
        }


        bool ComputeIntersection(const Segment& cutterSegment, const Segment& polygonEdge, list <Point>& newPoints, unsigned int& appEdge, list <Point>& intersectionPointsList, int& numPoints);



};

}

#endif


