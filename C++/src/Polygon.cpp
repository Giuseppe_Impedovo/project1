#include "Polygon.hpp"
#include "CutPolygon.hpp"

using namespace CutPolygonNamespace;
namespace PolygonNamespace{


double PolygonNamespace::Polygon::ComputeArea()
{
    double area = 0;
    int numVert = pPoints.size();
    for(unsigned int i = 0; i < (unsigned int)numVert; i++)
        area += 0.5*(pPoints[i]._x * pPoints[(i+1) % numVert]._y - pPoints[(i+1) % numVert]._x * pPoints[i]._y);

    return area;
}

bool Polygon::PointInPolygon(vector<Point> &points)
{
    bool puntoInterno = true;
    double controllo;
    int numVert = pPoints.size();
    for(unsigned int i = 0; i < points.size(); i++)
    {
        for(int j = 0; j < numVert; j++)
        {
            j = j % pPoints.size();
            controllo = (pPoints[(j+1) % numVert]._x - pPoints[j]._x)*(points[i]._y - pPoints[j]._y) - (pPoints[(j+1) % numVert]._y - pPoints[j]._y)*(points[i]._x - pPoints[j]._x);
            if(controllo < - toleranceEquality)
                puntoInterno = false;
        }

    }
    return puntoInterno;
}

void Polygon::Traslate(const double &x,const double &y)
{
    for(unsigned int k = 0; k < pPoints.size(); k++)
    {
        pPoints[k]._x += x;
        pPoints[k]._y += y;
    }
    if (boundingBox.size() != 0)
    {
        for(unsigned int h = 0; h < 4; h++)
        {
            boundingBox[h]._x += x;
            boundingBox[h]._y += y;
        }
    }
}

void Polygon::ComputeBoundingBox()
{
    boundingBox.reserve(4);
    double minX = pPoints[0]._x;
    double maxX = pPoints[0]._x;
    double minY = pPoints[0]._y;
    double maxY = pPoints[0]._y;
    for(unsigned int i = 0; i < (unsigned int)pPoints.size(); i++)
    {
        if(pPoints[i]._x < minX)
            minX = pPoints[i]._x;
        if(pPoints[i]._x > maxX)
            maxX = pPoints[i]._x;
        if(pPoints[i]._y < minY)
            minY = pPoints[i]._y;
        if(pPoints[i]._y > maxY)
            maxY = pPoints[i]._y;
    }
    boundingBox.push_back(Point(minX,minY));
    boundingBox.push_back(Point(maxX,minY));
    boundingBox.push_back(Point(maxX,maxY));
    boundingBox.push_back(Point(minX,maxY));
}

ReferenceElement ReferenceElement::Traslate(const double &x,const double &y)
{
    ReferenceElement newReferenceElement;
    newReferenceElement.polygons.reserve(polygons.size());
    newReferenceElement.boundingBox.reserve(4);
    for(unsigned int i = 0; i < 4; i++)    
        newReferenceElement.boundingBox.push_back(Point(boundingBox[i]._x + x,boundingBox[i]._y + y));

    for(unsigned int j = 0; j < polygons.size(); j++)
    {
            polygons[j].Traslate(x,y);
            newReferenceElement.polygons.push_back(polygons[j]);
            polygons[j].Traslate(-x,-y);
    }

    for (list<Point>::iterator it = addPoints.begin(); it != addPoints.end(); it++)
        newReferenceElement.addPoints.push_back(Point(it->_x + x, it->_y + y));

    return newReferenceElement;
}

void ReferenceElement::CreateReferenceElement(Polygon &poly, vector<Point> &bBox)
{
    boundingBox.reserve(4);
    for(unsigned int k = 0; k < 4; k++)
        boundingBox.push_back(bBox[k]);
    polygons.push_back(poly);

    // for per vedere e aggiungere i punti aggiuntivi per mesh conforme
    double controllo;
    for(unsigned int i = 0; i < poly.pPoints.size(); i++)
    {
        for(unsigned int j = 0; j < 4; j++)
        {
            controllo = (boundingBox[(j+1) % 4]._x - boundingBox[j]._x)*(poly.pPoints[i]._y - boundingBox[j]._y) - (boundingBox[(j+1) % 4]._y - boundingBox[j]._y)*(poly.pPoints[i]._x - boundingBox[j]._x);
            if(abs(controllo) < toleranceEquality)
            {
                poly.pPoints[i].bordo = true;
                poly.pPoints[i].numBordo = j;
                if(poly.pPoints[i]._x == boundingBox[0]._x)
                    addPoints.push_back(Point(boundingBox[2]._x, poly.pPoints[i]._y));
                if(poly.pPoints[i]._x == boundingBox[2]._x)
                    addPoints.push_back(Point(boundingBox[0]._x, poly.pPoints[i]._y));
                if(poly.pPoints[i]._y == boundingBox[0]._y)
                    addPoints.push_back(Point(poly.pPoints[i]._x, boundingBox[3]._y));
                if(poly.pPoints[i]._y == boundingBox[3]._y)
                    addPoints.push_back(Point(poly.pPoints[i]._x, boundingBox[0]._y));
            }
        }
    }

    // for per capire i poligoni interni del RefEl
    int miniPol = 0;
    Intersector intersecatore;
    CutPolygon cutPolygon(intersecatore);
    Polygon tmpPolygon(cutPolygon);
    for(int i = poly.pPoints.size() - 1; i >= 0; i--)
    {
        if(poly.pPoints[i].bordo == true)
        {
            tmpPolygon.pPoints.push_back(poly.pPoints[i]);
            miniPol++;
        }
        else if(poly.pPoints[i].bordo == false && miniPol == 1)
            tmpPolygon.pPoints.push_back(poly.pPoints[i]);
        if(miniPol == 2)
        {
            tmpPolygon.pPoints.push_back(boundingBox[(poly.pPoints[i].numBordo + 1) % 4]);
            polygons.push_back(tmpPolygon);
            tmpPolygon.pPoints.clear();
            i++;
            miniPol = 0;
        }
    }
    int i = poly.pPoints.size() - 1;
    for(i; i >= 0 && !poly.pPoints[i].bordo; i--)
    {
        tmpPolygon.pPoints.push_back(poly.pPoints[i]);
    }
    tmpPolygon.pPoints.push_back(poly.pPoints[i]);
    tmpPolygon.pPoints.push_back(boundingBox[(poly.pPoints[i].numBordo + 1) % 4]);
    polygons.push_back(tmpPolygon);
}

vector<ReferenceElement> ReferenceElement::CreateMesh(Polygon &domain, vector<ReferenceElement> &cells)
{
    // ciclo per capire quale tra i punti del poligono è in basso a sinistra e traslare nell'origine
    unsigned int indice = 0;
    for(unsigned int i = 1; i < (unsigned int)domain.pPoints.size(); i++)
    {
        if(domain.pPoints[i]._x <= domain.pPoints[indice]._x && domain.pPoints[i]._y <= domain.pPoints[indice]._y)
            indice = i;
    }
    domain.Traslate(-domain.pPoints[indice]._x,-domain.pPoints[indice]._y);
    ReferenceElement firstRefEl = Traslate(-boundingBox[0]._x,-boundingBox[0]._y);

    // le prossime righe servono per capire quanti RefEl entreranno
    indice = (indice + 1) % 4;
    int toRight = domain.pPoints[indice]._x / firstRefEl.boundingBox[1]._x;
    if(abs((domain.pPoints[indice]._x / firstRefEl.boundingBox[1]._x) - (double)toRight) < toleranceEquality)
        toRight--;
    indice = (indice + 1) % 4;
    int toUp = domain.pPoints[indice]._y / firstRefEl.boundingBox[3]._y;
    if(domain.pPoints[indice]._y / firstRefEl.boundingBox[3]._y - (double)toUp == 0)
        toUp--;
    cells.reserve((toUp+1) * (toRight+1));

    // aggiungiamo le celle che in base ai controlli di sopra siamo sicuri non vengano tagliate dai bordi del dominio
    for(unsigned int i = 0; i < (unsigned int)toUp; i++)
    {
        for(unsigned int j = 0; j < (unsigned int)toRight; j++)
            cells.push_back(firstRefEl.Traslate(j*firstRefEl.boundingBox[1]._x, i*firstRefEl.boundingBox[3]._y));
    }

    // taglio dei poligoni di sopra che sono tagliati solo dalla linea orizzontale, eccetto per l'ultimo in alto a destra
    ReferenceElement tmpRefEl;
    list<Polygon> finalCellPoly;
    Segment horizontalSeg = Segment(Point(domain.pPoints[3]._x - 1, domain.pPoints[3]._y), Point(domain.pPoints[2]._x + 100, domain.pPoints[2]._y));
    Segment verticalSeg = Segment(Point(domain.pPoints[1]._x , domain.pPoints[1]._y - 1), Point(domain.pPoints[2]._x, domain.pPoints[2]._y + 100));
    double controllo;
    list<Polygon> tmpPoly;

    for(int i = 0; i < toRight + 1; i++)
    {
        tmpRefEl = firstRefEl.Traslate(i*firstRefEl.boundingBox[1]._x, toUp*firstRefEl.boundingBox[3]._y);
        for(unsigned int j = 0; j < tmpRefEl.polygons.size(); j++) //ciclo per tagliare i poligoni di ogni cella
        {
            Intersector intersecatore;
            CutPolygon cutPolygon(intersecatore);
            for(unsigned int k = 0; k < tmpRefEl.polygons[j].pPoints.size(); k++)
                tmpRefEl.polygons[j].verticesVec.push_back(k);
            cutPolygon.Cut(tmpRefEl.polygons[j].pPoints, tmpRefEl.polygons[j].verticesVec, horizontalSeg);
            if(i != toRight)
            {
                //tra quelli tagliati da Cut() prendo solo i poligoni che sono nel dominio
                for (list<Polygon>::iterator it = cutPolygon.cuttedPolygon.begin(); it != cutPolygon.cuttedPolygon.end(); it++)
                {
                    controllo = (horizontalSeg.To._x - horizontalSeg.From._x)*(it->pPoints[0]._y - horizontalSeg.From._y) - (horizontalSeg.To._y - horizontalSeg.From._y)*(it->pPoints[0]._x - horizontalSeg.From._x);
                    if(abs(controllo) < toleranceEquality)
                        controllo = (horizontalSeg.To._x - horizontalSeg.From._x)*(it->pPoints[1]._y - horizontalSeg.From._y) - (horizontalSeg.To._y - horizontalSeg.From._y)*(it->pPoints[1]._x - horizontalSeg.From._x);
                    if(abs(controllo) < toleranceEquality)
                        controllo = (horizontalSeg.To._x - horizontalSeg.From._x)*(it->pPoints[2]._y - horizontalSeg.From._y) - (horizontalSeg.To._y - horizontalSeg.From._y)*(it->pPoints[2]._x - horizontalSeg.From._x);
                    if(controllo < - toleranceEquality)
                        tmpRefEl.miniPolygons.push_back(*it);
                }
            }
            //distinguo dall'ultimo in alto a destra che lo pushiamo in una lista di poligoni temporanea
            else
            {
                for (list<Polygon>::iterator it = cutPolygon.cuttedPolygon.begin(); it != cutPolygon.cuttedPolygon.end(); it++)
                {
                    controllo = (horizontalSeg.To._x - horizontalSeg.From._x)*(it->pPoints[0]._y - horizontalSeg.From._y) - (horizontalSeg.To._y - horizontalSeg.From._y)*(it->pPoints[0]._x - horizontalSeg.From._x);
                    if(abs(controllo) < toleranceEquality)
                        controllo = (horizontalSeg.To._x - horizontalSeg.From._x)*(it->pPoints[1]._y - horizontalSeg.From._y) - (horizontalSeg.To._y - horizontalSeg.From._y)*(it->pPoints[1]._x - horizontalSeg.From._x);
                    if(abs(controllo) < toleranceEquality)
                        controllo = (horizontalSeg.To._x - horizontalSeg.From._x)*(it->pPoints[2]._y - horizontalSeg.From._y) - (horizontalSeg.To._y - horizontalSeg.From._y)*(it->pPoints[2]._x - horizontalSeg.From._x);
                    if(controllo < - toleranceEquality)
                        finalCellPoly.push_back(*it);
                }
            }
        }
        if(i != toRight)
            cells.push_back(tmpRefEl);
    }

    // qui tagliamo l'ultima cella anche con il segmento verticale
    for(list<Polygon>::iterator it = finalCellPoly.begin(); it != finalCellPoly.end(); it++)
    {
        Intersector intersecatore;
        CutPolygon cutPolygon(intersecatore);
        cutPolygon.Cut(it->pPoints, it->verticesVec, verticalSeg);
        for (list<Polygon>::iterator it = cutPolygon.cuttedPolygon.begin(); it != cutPolygon.cuttedPolygon.end(); it++)
        {
            controllo = (verticalSeg.To._x - verticalSeg.From._x)*(it->pPoints[0]._y - verticalSeg.From._y) - (verticalSeg.To._y - verticalSeg.From._y)*(it->pPoints[0]._x - verticalSeg.From._x);
            if(abs(controllo) < toleranceEquality)
                controllo = (verticalSeg.To._x - verticalSeg.From._x)*(it->pPoints[1]._y - verticalSeg.From._y) - (verticalSeg.To._y - verticalSeg.From._y)*(it->pPoints[1]._x - verticalSeg.From._x);
            if(abs(controllo) < toleranceEquality)
                controllo = (verticalSeg.To._x - verticalSeg.From._x)*(it->pPoints[2]._y - verticalSeg.From._y) - (verticalSeg.To._y - verticalSeg.From._y)*(it->pPoints[2]._x - verticalSeg.From._x);
            if(controllo > toleranceEquality)
                tmpRefEl.miniPolygons.push_back(*it);
        }    
    }  
    cells.push_back(tmpRefEl);

    //mentre qui tagliamo le ultime celle che si trovano più a destra, le quali sono tagliate solo dal segmento verticale
    for(int i = 0; i < toUp; i++)
    {
        tmpRefEl = firstRefEl.Traslate(toRight*firstRefEl.boundingBox[1]._x, i*firstRefEl.boundingBox[3]._y);
        for(unsigned int j = 0; j < tmpRefEl.polygons.size(); j++)
        {
            for(unsigned int k = 0; k < tmpRefEl.polygons.size(); k++)
                tmpRefEl.polygons[j].verticesVec.push_back(k);
            Intersector intersecatore;
            CutPolygon cutPolygon(intersecatore);
            cutPolygon.Cut(tmpRefEl.polygons[j].pPoints, tmpRefEl.polygons[j].verticesVec, verticalSeg);
                for (list<Polygon>::iterator it = cutPolygon.cuttedPolygon.begin(); it != cutPolygon.cuttedPolygon.end(); it++)
                {
                    controllo = (verticalSeg.To._x - verticalSeg.From._x)*(it->pPoints[0]._y - verticalSeg.From._y) - (verticalSeg.To._y - verticalSeg.From._y)*(it->pPoints[0]._x - verticalSeg.From._x);
                    if(abs(controllo) < toleranceEquality)
                        controllo = (verticalSeg.To._x - verticalSeg.From._x)*(it->pPoints[1]._y - verticalSeg.From._y) - (verticalSeg.To._y - verticalSeg.From._y)*(it->pPoints[1]._x - verticalSeg.From._x);
                    if(abs(controllo) < toleranceEquality)
                        controllo = (verticalSeg.To._x - verticalSeg.From._x)*(it->pPoints[2]._y - verticalSeg.From._y) - (verticalSeg.To._y - verticalSeg.From._y)*(it->pPoints[2]._x - verticalSeg.From._x);
                    if(controllo > toleranceEquality)
                        tmpRefEl.miniPolygons.push_back(*it);
                }
        }
        cells.push_back(tmpRefEl);
    }
    return cells;
}

}

