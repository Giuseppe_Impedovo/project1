#ifndef POLYGON_H
#define POLYGON_H

#include "baseClass.hpp"
#include "Eigen"
#include "Intersector.hpp"
#include <iostream>
#include <list>
#include <vector>

using namespace std;
using namespace BaseClassNamespace;
using namespace IntersectorNamespace;

namespace PolygonNamespace{


class Polygon
{
    public:
        list<int> newPolygonVertices;
        list<Point> newPolygonPoints;
        vector<Point> pPoints;
        vector<Point> boundingBox;
        ICutPolygon& cutPolygon;
        vector<int> verticesVec;
    private:
        double toleranceEquality = 1.0E-7;
    public:
        bool operator==(const Polygon& polygon) const
        {
            return newPolygonVertices == polygon.newPolygonVertices;
        }
        Polygon(ICutPolygon& cutPolygon): cutPolygon(cutPolygon){}
        double ComputeArea();
        bool PointInPolygon(vector<Point>& points);
        void Traslate(const double& x,const double& y);
        void ComputeBoundingBox();

};

class ReferenceElement
{
    public:
        vector<Polygon> polygons;
        vector<Point> boundingBox;
        list<Point> addPoints;
        list<Polygon> miniPolygons;
    private:
        double toleranceEquality = 1.0E-7;

    public:
        ReferenceElement(){}
        ReferenceElement Traslate(const double& x,const double& y);
        void CreateReferenceElement(Polygon& poly, vector<Point>& bBox);
        vector<ReferenceElement> CreateMesh(Polygon& domain, vector<ReferenceElement>& cells);
};

}

#endif
