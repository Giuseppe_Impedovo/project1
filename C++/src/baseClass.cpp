#include "baseClass.hpp"

namespace BaseClassNamespace
{

Point::Point()
{
    _x = 0;
    _y = 0;
    appSegment = -2;
    paramCoord = 0;
    numVert = -1;
    bordo = false;
}

Point::Point(const double &x,const double &y)
{
    _x = x;
    _y = y;
    appSegment = -2;
    paramCoord = 0;
    numVert = -1;
    bordo = false;
}

Segment::Segment(const Point &from, const Point &to): From(from), To(to)
{
    intersection = false;
}



}
