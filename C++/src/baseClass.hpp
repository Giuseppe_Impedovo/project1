#ifndef BASECLASS_H
#define BASECLASS_H

#include <vector>
#include <iostream>
#include "Eigen"



using namespace std;
using namespace Eigen;


namespace BaseClassNamespace
{

class Point
{
    public:
        double _x;
        double _y;
        int appSegment;
        double paramCoord;
        int numVert;
        bool passed;
        int friendship;
        int enumTypeIntersection;
        //attributi secondo progetto
        bool bordo;
        int numBordo;

    private:
        double toleranceEquality = 1.0E-7;

    public:
        Point();
        Point(const double& x, const double& y);
        Point& operator=(const Point& point)
        {
            _x = point._x;
            _y = point._y;
            appSegment = point.appSegment;
            paramCoord = point.paramCoord;
            numVert = point.numVert;
            passed = point.passed;
            friendship = point.friendship;
            enumTypeIntersection = point.enumTypeIntersection;
            return *this;
        }
        bool operator==(const Point& point) const
        {
            bool uguaglianza = false;
            if(abs((point._x - _x)) < toleranceEquality && abs((point._y - _y)) < toleranceEquality)
                uguaglianza = true;
            return uguaglianza;
        }
};

class Segment
{
    public:
        Point From;
        Point To;
        bool intersection;
    private:
        double toleranceEquality = 1.0E-7;

    public:
        Segment(const Point& from,const Point& to);
        Segment& operator=(const Segment& segment)
        {
            From = segment.From;
            To = segment.To;
            return *this;
        }
        bool operator==(const Segment& segment) const
        {
            return From == segment.From && To == segment.To;
        }
};

class ICutPolygon
{
    public:
        virtual void Cut(const vector<Point>& points, const vector<int>& vertices, const Segment& segment) = 0;
        virtual void SavePolygon(const vector<Point>& points, const vector<int>& vertices) = 0;

};

class IIntersector
{
    public:
        virtual void SaveSegments(const Segment& cutterSegment, const Segment& polygonEdge) = 0;
        virtual void SetCutterSegment(const Vector2d& origin, const Vector2d& end) = 0;
        virtual void SetPolygonEdge(const Vector2d& origin, const Vector2d& end) = 0;
        virtual bool ComputeIntersection(const Segment& cutterSegment, const Segment& polygonEdge, list <Point>& newPoints, unsigned int& appEdge, list <Point>& intersectionPointsList, int& numPoints) = 0;
};

}


#endif
