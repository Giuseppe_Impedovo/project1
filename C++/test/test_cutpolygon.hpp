#ifndef __TEST_CUTPOLYGON_H
#define __TEST_CUTPOLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "CutPolygon.hpp"
#include "Polygon.hpp"

using namespace CutPolygonNamespace;
using namespace PolygonNamespace;
using namespace testing;
using namespace std;

namespace CutPolyogonTesting {

    class MockIntersector : public IIntersector
    {
      public:
        MOCK_METHOD2(SaveSegments, void(const Segment& cutterSegment, const Segment& polygonEdge));
        MOCK_METHOD2(SetCutterSegment, void(const Vector2d& origin, const Vector2d& end));
        MOCK_METHOD2(SetPolygonEdge, void(const Vector2d& origin, const Vector2d& end));
        MOCK_METHOD6(ComputeIntersection, bool(const Segment& cutterSegment, const Segment& polygonEdge, list <Point>& newPoints, unsigned int& appEdge, list <Point>& intersectionPointsList, int& numPoints));

    };
    TEST(TestCutPolygon, TestSavePolygon)
    {
        MockIntersector intersecatore;
        CutPolygonNamespace::CutPolygon cutPolygon(intersecatore);

        vector<Point> points;
        Point A = Point(1.0,1.0);
        Point B = Point(3.0,2.5);
        points.reserve(2);
        points.push_back(A);
        points.push_back(B);
        list<Point> controlList;
        controlList.push_back(A);
        controlList.push_back(B);
        Segment controlSeg = Segment(A,B);
        vector<Segment> controlVec;
        controlVec.push_back(controlSeg);
        vector<int> vertices = {0,1};

        try
        {
            cutPolygon.SavePolygon(points, vertices);
            EXPECT_EQ(cutPolygon.newPoints, controlList);
            EXPECT_EQ(cutPolygon.polygonEdges, controlVec);
        }
        catch (const exception& exception)
        {
            FAIL();
        }
    }

    TEST(TestCutPolygon, TestCut1)
    {
        MockIntersector intersecatore1;
        CutPolygonNamespace::CutPolygon cutPolygon(intersecatore1);

        Point A = Point(1.0,1.0);
        Point B = Point(5.0,1.0);
        Point C = Point(5.0,3.1);
        Point D = Point(1.0,3.1);
        Point E = Point(2.0,1.2);
        Point F = Point(4.0,3.0);
        Point inters1 = Point(1.78,1.0);
        inters1.numVert = 4;
        inters1.enumTypeIntersection = 1;
        inters1.appSegment = 0;
        inters1.paramCoord = -0.11111111;
        Point inters2 = Point(4.11,3.1);
        inters2.numVert = 5;
        inters2.enumTypeIntersection = 1;
        inters2.appSegment = 2;
        inters2.paramCoord = 1.0555556;
        vector<Point> initialPoints = {A, B, C, D};
        list<Point> newPoints1 = {A, B, C, D, inters1};
        list<Point> newPoints2 = {A, B, C, D, inters1, inters2};
        list<Point> intPoints1 = {inters1};
        list<Point> intPoints2 = {inters1, inters2};
        list<Point> finalPoints = {A, B, C, D, inters1, inters2, E, F};
        Polygon poligono1(cutPolygon);
        poligono1.newPolygonVertices = {0, 4, 6, 7, 5, 3};
        Polygon poligono2(cutPolygon);
        poligono2.newPolygonVertices = {1, 2, 5, 7, 6, 4};
        list<Polygon> finalCuttedPolygons = {poligono1, poligono2};
        unsigned int i = 0;
        int numP = 4;
        Segment initialSegment = Segment(E,F);
        vector<int> initialVertices = {0, 1, 2, 3};


        try
        {
            cutPolygon.SavePolygon(initialPoints, initialVertices);
            intersecatore1.ComputeIntersection(initialSegment, cutPolygon.polygonEdges[0], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP);
            EXPECT_CALL(intersecatore1, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[0], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP)).WillOnce(DoAll(testing::SetArgReferee<2>(newPoints1),
                                                                                                                                                                                          testing::SetArgReferee<4>(intPoints1),
                                                                                                                                                                                          testing::SetArgReferee<5>(5),
                                                                                                                                                                                          Return(true)));
            i++;
            numP++;
            EXPECT_CALL(intersecatore1, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[1], newPoints1, i, intPoints1, numP)).WillOnce(Return(false));
            i++;
            EXPECT_CALL(intersecatore1, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[2], newPoints1, i, intPoints1, numP)).WillOnce(DoAll(testing::SetArgReferee<2>(newPoints2),
                                                                                                                                                        testing::SetArgReferee<4>(intPoints2),
                                                                                                                                                        testing::SetArgReferee<5>(6),
                                                                                                                                                        Return(true)));
            i++;
            numP++;
            EXPECT_CALL(intersecatore1, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[3], newPoints2, i, intPoints2, numP)).WillOnce(Return(false));

            cutPolygon.newPoints.clear();
            cutPolygon.polygonEdges.clear();
            cutPolygon.Cut(initialPoints, initialVertices, initialSegment);
            EXPECT_EQ(cutPolygon.newPoints, finalPoints);
            EXPECT_EQ(cutPolygon.cuttedPolygon, finalCuttedPolygons);
        }
        catch (const exception& exception)
        {
            FAIL();
        }
    }
    TEST(TestCutPolygon, TestCut2)
    {
        MockIntersector intersecatore2;
        CutPolygonNamespace::CutPolygon cutPolygon(intersecatore2);

        Point A = Point(2.5,1.0);
        Point B = Point(4.0,2.1);
        Point C = Point(3.4,4.2);
        Point D = Point(1.6,4.2);
        Point E = Point(1.0,2.1);
        Point F = Point(1.4,2.75);
        Point G = Point(3.6,2.2);
        vector<Point> initialPoints = {A, B, C, D, E};
        Point inters1 = Point(4.0,2.1);
        inters1.numVert = 1;
        inters1.enumTypeIntersection = 2;
        inters1.appSegment = 0;
        inters1.paramCoord = 1.18182;
        Point inters2 = Point(4.0,2.1);
        inters2.numVert = 1;
        inters2.enumTypeIntersection = 0;
        inters2.appSegment = 1;
        inters2.paramCoord = 1.18182;
        Point inters3 = Point(1.2,2.8);
        inters3.numVert = 5;
        inters3.enumTypeIntersection = 1;
        inters3.appSegment = 3;
        inters3.paramCoord = -0.090909094;
        Segment initialSegment = Segment(F,G);
        vector<int> initialVertices = {0,1,2,3,4};
        list<Point> intPoints1 = {inters1};
        list<Point> intPoints2 = {inters1, inters2};
        list<Point> intPoints3 = {inters1, inters2, inters3};
        list<Point> newPoints1 = {A, B, C, D, E, inters3};
        list<Point> finalPoints = {A, B, C, D, E, inters3, F, G};
        Polygon poligono1(cutPolygon);
        poligono1.newPolygonVertices = {0, 1, 7, 6, 5, 4};
        Polygon poligono2(cutPolygon);
        poligono2.newPolygonVertices = {1, 2, 3, 5, 6, 7};
        list<Polygon> finalCuttedPolygons = {poligono1, poligono2};

        unsigned int i = 0;
        int numP = 5;



        try
        {
            cutPolygon.SavePolygon(initialPoints, initialVertices);
            EXPECT_CALL(intersecatore2, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[0], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP)).WillOnce(DoAll(testing::SetArgReferee<4>(intPoints1),
                                                                                                                                                                                          Return(true)));
            i++;
            EXPECT_CALL(intersecatore2, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[1], cutPolygon.newPoints, i, intPoints1 , numP)).WillOnce(DoAll(testing::SetArgReferee<4>(intPoints2),
                                                                                                                                                                    Return(true)));
            i++;
            EXPECT_CALL(intersecatore2, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[2], cutPolygon.newPoints, i, intPoints2, numP)).WillOnce(Return(false));
            i++;
            EXPECT_CALL(intersecatore2, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[3], cutPolygon.newPoints, i, intPoints2, numP)).WillOnce(DoAll(testing::SetArgReferee<2>(newPoints1),
                                                                                                                                                                   testing::SetArgReferee<4>(intPoints3),
                                                                                                                                                                   testing::SetArgReferee<5>(6),
                                                                                                                                                                   Return(true)));

            i++;
            numP++;
            EXPECT_CALL(intersecatore2, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[4], newPoints1, i, intPoints3, numP)).WillOnce(Return(false));
            cutPolygon.newPoints.clear();
            cutPolygon.polygonEdges.clear();
            cutPolygon.Cut(initialPoints, initialVertices, initialSegment);
            EXPECT_EQ(cutPolygon.newPoints, finalPoints);
            EXPECT_EQ(cutPolygon.cuttedPolygon, finalCuttedPolygons);
        }

        catch (const exception& exception)
        {
            FAIL();
        }
    }
    TEST(TestCutPolygon, TestCut3)
    {
        MockIntersector intersecatore3;
        CutPolygonNamespace::CutPolygon cutPolygon(intersecatore3);

        Point A = Point(1.5,1.0);
        Point B = Point(5.6,1.5);
        Point C = Point(5.5,4.8);
        Point D = Point(4.0,6.2);
        Point E = Point(3.2,4.2);
        Point F = Point(1.0,4.0);
        Point G = Point(2.0,3.7);
        Point H = Point(4.1,5.9);
        vector<Point> initialPoints = {A, B, C, D, E, F};
        Segment initialSegment = Segment(G,H);
        vector<int> initialVertices = {0,1,2,3,4,5};
        Point inters1 = Point(4.28,6.09);
        inters1.numVert = 6;
        inters1.enumTypeIntersection = 1;
        inters1.appSegment = 2;
        inters1.paramCoord = 1.0496795;
        Point inters2 = Point(3.72,5.50);
        inters2.numVert = 7;
        inters2.enumTypeIntersection = 1;
        inters2.appSegment = 3;
        inters2.paramCoord = 0.81967211;
        Point inters3 = Point(2.41,4.13);
        inters3.numVert = 8;
        inters3.enumTypeIntersection = 1;
        inters3.appSegment = 4;
        inters3.paramCoord = 0.19457014;
        Point inters4 = Point(1.19,2.85);
        inters4.numVert = 9;
        inters4.enumTypeIntersection = 1;
        inters4.appSegment = 5;
        inters4.paramCoord = -0.38513514;
        list<Point> newPoints1 = {A, B, C, D, E, F, inters1};
        list<Point> intPoints1 = {inters1};
        list<Point> newPoints2 = {A, B, C, D, E, F, inters1, inters2};
        list<Point> intPoints2 = {inters1, inters2};
        list<Point> newPoints3 = {A, B, C, D, E, F, inters1, inters2, inters3};
        list<Point> intPoints3 = {inters1, inters2, inters3};
        list<Point> newPoints4 = {A, B, C, D, E, F, inters1, inters2, inters3, inters4};
        list<Point> intPoints4 = {inters1, inters2, inters3, inters4};
        list<Point> finalPoints = {A, B, C, D, E, F, inters1, inters2, inters3, inters4, G, H};
        Polygon poligono1(cutPolygon);
        poligono1.newPolygonVertices = {0, 1, 2, 6, 11, 7, 4, 8, 10, 9};
        Polygon poligono2(cutPolygon);
        poligono2.newPolygonVertices = {3, 7, 11, 6};
        Polygon poligono3(cutPolygon);
        poligono3.newPolygonVertices = {5, 9, 10, 8};
        list<Polygon> finalCuttedPolygons = {poligono1, poligono2, poligono3};
        unsigned int i = 0;
        int numP = 6;

        try
        {
            cutPolygon.SavePolygon(initialPoints, initialVertices);
            EXPECT_CALL(intersecatore3, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[0], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP)).WillOnce(Return(false));
            i++;
            EXPECT_CALL(intersecatore3, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[1], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP)).WillOnce(Return(false));
            i++;
            EXPECT_CALL(intersecatore3, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[2], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP)).WillOnce(DoAll(testing::SetArgReferee<2>(newPoints1),
                                                                                                                                                                                          testing::SetArgReferee<4>(intPoints1),
                                                                                                                                                                                          testing::SetArgReferee<5>(7),
                                                                                                                                                                                          Return(true)));
            i++;
            numP++;
            EXPECT_CALL(intersecatore3, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[3], newPoints1, i, intPoints1, numP)).WillOnce(DoAll(testing::SetArgReferee<2>(newPoints2),
                                                                                                                                                         testing::SetArgReferee<4>(intPoints2),
                                                                                                                                                         testing::SetArgReferee<5>(8),
                                                                                                                                                         Return(true)));
            i++;
            numP++;
            EXPECT_CALL(intersecatore3, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[4], newPoints2, i, intPoints2, numP)).WillOnce(DoAll(testing::SetArgReferee<2>(newPoints3),
                                                                                                                                                         testing::SetArgReferee<4>(intPoints3),
                                                                                                                                                         testing::SetArgReferee<5>(9),
                                                                                                                                                         Return(true)));
            i++;
            numP++;
            EXPECT_CALL(intersecatore3, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[5], newPoints3, i, intPoints3, numP)).WillOnce(DoAll(testing::SetArgReferee<2>(newPoints4),
                                                                                                                                                         testing::SetArgReferee<4>(intPoints4),
                                                                                                                                                         testing::SetArgReferee<5>(10),
                                                                                                                                                         Return(true)));

            cutPolygon.newPoints.clear();
            cutPolygon.polygonEdges.clear();
            cutPolygon.Cut(initialPoints, initialVertices, initialSegment);
            EXPECT_EQ(cutPolygon.newPoints, finalPoints);
            EXPECT_EQ(cutPolygon.cuttedPolygon, finalCuttedPolygons);

        }
        catch (const exception& exception)
        {
            FAIL();
        }
    }
    TEST(TestCutPolygon, TestCut4Conv)
    {
        MockIntersector intersecatore4;
        CutPolygonNamespace::CutPolygon cutPolygon(intersecatore4);

        Point A = Point(0.0,-2.0);
        Point B = Point(2.0,0.0);
        Point C = Point(0.0,2.0);
        Point D = Point(-2.0,0.0);
        Point E = Point(-3.0,-1.0);
        Point F = Point(1.0,3.0);
        vector<Point> initialPoints = {A, B, C, D};
        Segment initialSegment = Segment(E,F);
        vector<int> initialVertices = {0,1,2,3};
        Point inters1 = Point(0.0,2.0);
        inters1.numVert = 2;
        inters1.enumTypeIntersection = 2;
        inters1.appSegment = 1;
        inters1.paramCoord = 0.75;
        Point inters2 = Point(-2.0,0.0);
        inters2.numVert = 3;
        inters2.enumTypeIntersection = 0;
        inters2.appSegment = 3;
        inters2.paramCoord = 0.25;
        list<Point> intPoints1 = {inters1};
        list<Point> intPoints2 = {inters1, inters2};
        list<Point> finalPoints = {A, B, C, D};
        Polygon poligono1(cutPolygon);
        poligono1.newPolygonVertices = {0, 1, 2, 3};
        list<Polygon> finalCuttedPolygons = {poligono1};
        unsigned int i = 0;
        int numP = 4;

        try
        {
            cutPolygon.SavePolygon(initialPoints, initialVertices);
            EXPECT_CALL(intersecatore4, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[0], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP)).WillOnce(Return(false));
            i++;
            EXPECT_CALL(intersecatore4, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[1], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP)).WillOnce(DoAll(testing::SetArgReferee<4>(intPoints1),
                                                                                                                                                                                          Return(true)));
            i++;
            EXPECT_CALL(intersecatore4, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[2], cutPolygon.newPoints, i, intPoints1, numP)).WillOnce(Return(true));
            i++;
            EXPECT_CALL(intersecatore4, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[3], cutPolygon.newPoints, i, intPoints1, numP)).WillOnce(DoAll(testing::SetArgReferee<4>(intPoints2),
                                                                                                                                                                   Return(true)));
            cutPolygon.newPoints.clear();
            cutPolygon.polygonEdges.clear();
            cutPolygon.Cut(initialPoints, initialVertices, initialSegment);
            EXPECT_EQ(cutPolygon.newPoints, finalPoints);
            EXPECT_EQ(cutPolygon.cuttedPolygon, finalCuttedPolygons);

        }
        catch (const exception& exception)
        {
            FAIL();
        }
    }
    TEST(TestCutPolygon, TestCut5Conc)
        {
            MockIntersector intersecatore5;
            CutPolygonNamespace::CutPolygon cutPolygon(intersecatore5);

            Point A = Point(2.0,-2.0);
            Point B = Point(0.0,-1.0);
            Point C = Point(3.0,1.0);
            Point D = Point(0.0,2.0);
            Point E = Point(3.0,2.0);
            Point F = Point(3.0,3.0);
            Point G = Point(-1.0,3.0);
            Point H = Point(-3.0,1.0);
            Point I = Point(0.0,0.0);
            Point J = Point(-3.0,-2.0);
            Point K = Point(-4.0,-4.0);
            Point L = Point(4.0,4.0);
            vector<Point> initialPoints = {A, B, C, D, E, F, G, H, I, J};
            Segment initialSegment = Segment(K,L);
            vector<int> initialVertices = {0,1,2,3,4,5,6,7,8,9};
            Point inters1 = Point(1.5,1.5);
            inters1.numVert = 10;
            inters1.enumTypeIntersection = 1;
            inters1.appSegment = 2;
            inters1.paramCoord = 0.6875;
            Point inters2 = Point(2.0,2.0);
            inters2.numVert = 11;
            inters2.enumTypeIntersection = 1;
            inters2.appSegment = 3;
            inters2.paramCoord = 0.75;
            Point inters3 = Point(3.0,3.0);
            inters3.numVert = 5;
            inters3.enumTypeIntersection = 2;
            inters3.appSegment = 4;
            inters3.paramCoord = 0.875;
            Point inters4 = Point(3.0,3.0);
            inters4.numVert = 5;
            inters4.enumTypeIntersection = 0;
            inters4.appSegment = 5;
            inters4.paramCoord = 0.875;
            Point inters5 = Point(0.0,0.0);
            inters5.numVert = 8;
            inters5.enumTypeIntersection = 2;
            inters5.appSegment = 7;
            inters5.paramCoord = 0.5;
            Point inters6 = Point(0.0,0.0);
            inters6.numVert = 8;
            inters6.enumTypeIntersection = 0;
            inters6.appSegment = 8;
            inters6.paramCoord = 0.5;
            Point inters7 = Point(-2.0,-2.0);
            inters7.numVert = 12;
            inters7.enumTypeIntersection = 1;
            inters7.appSegment = 9;
            inters7.paramCoord = 0.25;
            unsigned int i = 0;
            int numP = 10;
            list<Point> newPoints1 = {A, B, C, D, E, F, G, H, I, J, inters1};
            list<Point> newPoints2 = {A, B, C, D, E, F, G, H, I, J, inters1, inters2};
            list<Point> finalPoints = {A, B, C, D, E, F, G, H, I, J, inters1, inters2, inters7};
            list<Point> intPoints1 = {inters1};
            list<Point> intPoints2 = {inters1, inters2};
            list<Point> intPoints3 = {inters1, inters2, inters3};
            list<Point> intPoints4 = {inters1, inters2, inters3, inters4};
            list<Point> intPoints5 = {inters1, inters2, inters3, inters4, inters5};
            list<Point> intPoints6 = {inters1, inters2, inters3, inters4, inters5, inters6};
            list<Point> intPoints7 = {inters1, inters2, inters3, inters4, inters5, inters6, inters7};
            Polygon poligono1(cutPolygon);
            poligono1.newPolygonVertices = {0, 1, 2, 10, 8, 12};
            Polygon poligono2(cutPolygon);
            poligono2.newPolygonVertices = {3, 11, 5, 6, 7, 8, 10};
            Polygon poligono3(cutPolygon);
            poligono3.newPolygonVertices = {4, 5, 11};
            Polygon poligono4(cutPolygon);
            poligono4.newPolygonVertices = {9, 12, 8};
            list<Polygon> finalCuttedPolygons = {poligono1, poligono2, poligono3, poligono4};

            try
            {
                cutPolygon.SavePolygon(initialPoints, initialVertices);

                EXPECT_CALL(intersecatore5, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[0], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP)).WillOnce(Return(false));
                i++;
                EXPECT_CALL(intersecatore5, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[1], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP)).WillOnce(Return(false));
                i++;
                EXPECT_CALL(intersecatore5, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[2], cutPolygon.newPoints, i, cutPolygon.intersectionPointsList, numP)).WillOnce(DoAll(testing::SetArgReferee<2>(newPoints1),
                                                                                                                                                                                              testing::SetArgReferee<4>(intPoints1),
                                                                                                                                                                                              testing::SetArgReferee<5>(11),
                                                                                                                                                                                              Return(true)));
                i++;
                numP++;
                EXPECT_CALL(intersecatore5, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[3], newPoints1, i, intPoints1, numP)).WillOnce(DoAll(testing::SetArgReferee<2>(newPoints2),
                                                                                                                                                             testing::SetArgReferee<4>(intPoints2),
                                                                                                                                                             testing::SetArgReferee<5>(12),
                                                                                                                                                             Return(true)));
                i++;
                numP++;
                EXPECT_CALL(intersecatore5, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[4], newPoints2, i, intPoints2, numP)).WillOnce(DoAll(testing::SetArgReferee<4>(intPoints3),
                                                                                                                                                                       Return(true)));
                i++;
                EXPECT_CALL(intersecatore5, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[5], newPoints2, i, intPoints3, numP)).WillOnce(DoAll(testing::SetArgReferee<4>(intPoints4),
                                                                                                                                                                       Return(true)));
                i++;
                EXPECT_CALL(intersecatore5, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[6], newPoints2, i, intPoints4, numP)).WillOnce(Return(false));
                i++;
                EXPECT_CALL(intersecatore5, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[7], newPoints2, i, intPoints4, numP)).WillOnce(DoAll(testing::SetArgReferee<4>(intPoints5),
                                                                                                                                                                       Return(true)));
                i++;
                EXPECT_CALL(intersecatore5, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[8], newPoints2, i, intPoints5, numP)).WillOnce(DoAll(testing::SetArgReferee<4>(intPoints6),
                                                                                                                                                                       Return(true)));
                i++;
                EXPECT_CALL(intersecatore5, ComputeIntersection(initialSegment, cutPolygon.polygonEdges[9], newPoints2, i, intPoints6, numP)).WillOnce(DoAll(testing::SetArgReferee<2>(finalPoints),
                                                                                                                                                             testing::SetArgReferee<4>(intPoints7),
                                                                                                                                                             testing::SetArgReferee<5>(13),
                                                                                                                                                             Return(true)));
                cutPolygon.newPoints.clear();
                cutPolygon.polygonEdges.clear();
                cutPolygon.Cut(initialPoints, initialVertices, initialSegment);
                EXPECT_EQ(cutPolygon.newPoints, finalPoints);
                EXPECT_EQ(cutPolygon.cuttedPolygon, finalCuttedPolygons);
            }
            catch (const exception& exception)
            {
                FAIL();
            }
        }

}

#endif // __TEST_CUTPOLYGON_H
