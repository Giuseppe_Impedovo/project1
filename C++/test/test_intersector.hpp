#ifndef __TEST_INTERSECTOR_H
#define __TEST_INTERSECTOR_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "Intersector.hpp"

using namespace IntersectorNamespace;
using namespace testing;
using namespace std;

namespace IntersectorTesting {

    TEST(TestIntersector, TestSaveAll)
    {
        Intersector intersector;
        Point A = Point(1.0,1.0);
        Point B = Point(3.0,1.0);
        Point C = Point(2.0,0.0);
        Point D = Point(2.0,2.0);
        Segment cSegment = Segment(A,B);
        Segment pSegment = Segment(C,D);
        Vector2d control1 = {2,0};
        Vector2d control2 = {1,1};
        Vector2d control3 = {0,-2};
        Vector2d control4 = {1,-1};

        try
        {
          intersector.SaveSegments(cSegment, pSegment);
          EXPECT_EQ(intersector.matrixTangentVector.col(0), control1);
          EXPECT_EQ(intersector.originFirstSegment, control2);
          EXPECT_EQ(intersector.matrixTangentVector.col(1), control3);
          EXPECT_EQ(intersector.rightHandSide, control4);
        }
        catch (const exception& exception)
        {
          FAIL();
        }
    }


    TEST(TestIntersector, TestParallelIntersectionOnSegment)
    {
        Intersector intersector;
        Point A = Point(0.0,0.0);
        Point B = Point(4.0,0.0);
        Point C = Point(1.0,0.0);
        Point D = Point(2.0,0.0);
        Segment cSegment = Segment(A,B);
        Segment pSegment = Segment(C,D);
        list<Point> cNewPoints;
        unsigned int controlAppEdge = 0;
        list<Point> controlInterPoints;
        int controlNumPoints = 0;
        intersector.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, controlNumPoints);
        EXPECT_TRUE(intersector.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, controlNumPoints));
        EXPECT_EQ(intersector.type, Intersector::IntersectionParallelOnSegment);
        EXPECT_FLOAT_EQ(0.25, intersector.resultParametricCoordinates(0));
        EXPECT_FLOAT_EQ(0.5, intersector.resultParametricCoordinates(1));

    }

    TEST(TestIntersector, TestParallelIntersectionOnLine)
    {
        Intersector intersector;
        Point A = Point(0.0,0.0);
        Point B = Point(4.0,0.0);
        Point C = Point(1.0,0.0);
        Point D = Point(2.0,0.0);
        Segment cSegment = Segment(A,C);
        Segment pSegment = Segment(D,B);
        list<Point> cNewPoints;
        unsigned int controlAppEdge = 0;
        list<Point> controlInterPoints;
        int controlNumPoints = 0;

        EXPECT_TRUE(intersector.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, controlNumPoints));
        EXPECT_EQ(intersector.type, Intersector::IntersectionParallelOnLine);
        EXPECT_FLOAT_EQ(2.0, intersector.resultParametricCoordinates(0));
        EXPECT_FLOAT_EQ(4.0, intersector.resultParametricCoordinates(1));

    }

    TEST(TestIntersector, TestOnSegmentIntersectionInner)
    {
        Intersector intersector;
        Point A = Point(0.0,2.0);
        Point B = Point(0.0,4.0);
        Point C = Point(2.0,3.0);
        Point D = Point(-2.0,3.0);
        Segment cSegment = Segment(C,D);
        Segment pSegment = Segment(A,B);
        list<Point> cNewPoints;
        unsigned int controlAppEdge = 0;
        list<Point> controlInterPoints;
        int controlNumPoints = 0;

        EXPECT_TRUE(intersector.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, controlNumPoints));
        EXPECT_EQ(intersector.type, Intersector::IntersectionOnSegment);
        EXPECT_FLOAT_EQ(0.5, intersector.resultParametricCoordinates(0));
        EXPECT_FLOAT_EQ(0.5, intersector.resultParametricCoordinates(1));
        EXPECT_EQ(controlNumPoints, 1);
        EXPECT_EQ(controlInterPoints.size(), 1);
        EXPECT_EQ(cNewPoints.begin()->_x, 0);
        EXPECT_EQ(cNewPoints.begin()->_y, 3);
        EXPECT_EQ(cNewPoints.begin()->appSegment, controlAppEdge);
        EXPECT_EQ(cNewPoints.begin()->paramCoord, intersector.resultParametricCoordinates(0));
        EXPECT_EQ(cNewPoints.begin()->numVert, controlNumPoints - 1);
        EXPECT_EQ(cNewPoints.begin()->enumTypeIntersection, 1);

    }


    TEST(TestIntersector, TestOnLineIntersectionBegin)
    {
        Intersector intersector;
        Point A = Point(1.0,0.0);
        Point B = Point(5.0,0.0);
        Point C = Point(1.0,-6.0);
        Point D = Point(1.0,-2.0);
        A.numVert = 0;
        Segment cSegment = Segment(C,D);
        Segment pSegment = Segment(A,B);
        list<Point> cNewPoints;
        unsigned int controlAppEdge = 0;
        list<Point> controlInterPoints;
        int controlNumPoints = 0;

        EXPECT_TRUE(intersector.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, controlNumPoints));
        EXPECT_EQ(intersector.type, Intersector::IntersectionOnLine);
        EXPECT_FLOAT_EQ(1.5, intersector.resultParametricCoordinates(0));
        EXPECT_FLOAT_EQ(0.0, intersector.resultParametricCoordinates(1));
        EXPECT_EQ(controlNumPoints, 0);
        EXPECT_EQ(controlInterPoints.size(), 1);
        EXPECT_EQ(controlInterPoints.begin()->_x, 1);
        EXPECT_EQ(controlInterPoints.begin()->_y, 0);
        EXPECT_EQ(controlInterPoints.begin()->appSegment, controlAppEdge);
        EXPECT_EQ(controlInterPoints.begin()->paramCoord, intersector.resultParametricCoordinates(0));
        EXPECT_EQ(controlInterPoints.begin()->numVert, A.numVert);
        EXPECT_EQ(controlInterPoints.begin()->enumTypeIntersection, 0);

    }

    TEST(TestIntersector, TestOnSegmentIntersectionEnd)
    {
        Intersector intersector;
        Point A = Point(1.0,0.0);
        Point B = Point(5.0,0.0);
        Point C = Point(5.0,-6.0);
        Point D = Point(5.0,6.0);
        B.numVert = 0;
        Segment cSegment = Segment(C,D);
        Segment pSegment = Segment(A,B);
        list<Point> cNewPoints;
        unsigned int controlAppEdge = 0;
        list<Point> controlInterPoints;
        int controlNumPoints = 0;

        EXPECT_TRUE(intersector.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, controlNumPoints));
        EXPECT_EQ(intersector.type, Intersector::IntersectionOnSegment);
        EXPECT_FLOAT_EQ(0.5, intersector.resultParametricCoordinates(0));
        EXPECT_FLOAT_EQ(1.0, intersector.resultParametricCoordinates(1));
        EXPECT_EQ(controlNumPoints, 0);
        EXPECT_EQ(controlInterPoints.size(), 1);
        EXPECT_EQ(controlInterPoints.begin()->_x, 5);
        EXPECT_EQ(controlInterPoints.begin()->_y, 0);
        EXPECT_EQ(controlInterPoints.begin()->appSegment, controlAppEdge);
        EXPECT_EQ(controlInterPoints.begin()->paramCoord, intersector.resultParametricCoordinates(0));
        EXPECT_EQ(controlInterPoints.begin()->numVert, B.numVert);
        EXPECT_EQ(controlInterPoints.begin()->enumTypeIntersection, 2);

    }
    TEST(TestIntersector, TestNoIntersection)
    {
        Intersector intersector;
        Point A = Point(1.0,0.0);
        Point B = Point(5.0,0.0);
        Point C = Point(1.0,1.0);
        Point D = Point(5.0,1.0);
        Segment cSegment = Segment(C,D);
        Segment pSegment = Segment(A,B);
        list<Point> cNewPoints;
        unsigned int controlAppEdge = 0;
        list<Point> controlInterPoints;
        int controlNumPoints = 0;

        EXPECT_FALSE(intersector.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, controlNumPoints));
        EXPECT_EQ(intersector.type, Intersector::NoIntersection);

    }







}

#endif // __TEST_INTERSECTOR_H
