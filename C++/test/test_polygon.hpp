#ifndef __TEST_POLYGON_H
#define __TEST_POLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "CutPolygon.hpp"
#include "Polygon.hpp"


using namespace CutPolygonNamespace;
using namespace PolygonNamespace;
using namespace testing;
using namespace std;

namespace PolygonTesting {

    TEST(TestPolygon, TestComputeArea)
    {
        Intersector intersecatore;
        CutPolygonNamespace::CutPolygon cutPolygon(intersecatore);
        Polygon poly(cutPolygon);
        poly.pPoints = {Point(1.0,1.0), Point(5.0,1.0), Point(5.0,5.0), Point(1.0,5.0)};
        double area = 16.00;
        try
        {
          EXPECT_EQ(area, poly.ComputeArea());
        }
        catch (const exception& exception)
        {
          FAIL();
        }
    }

    TEST(TestPolygon, TestCreateMesh)
    {
        Point A = Point(1.5,1.0);
        Point B = Point(5.6,1.5);
        Point C = Point(5.5,4.8);
        Point D = Point(4.0,6.2);
        Point E = Point(3.2,4.2);
        Point F = Point(1.0,4.0);
        Intersector intersecatore;
        CutPolygon cutPolygon(intersecatore);
        Polygon poly(cutPolygon);
        poly.pPoints = {A, B, C, D, E, F};
        poly.ComputeBoundingBox();
        Polygon domain(cutPolygon);
        domain.pPoints = {Point(0.0,0.0), Point(19.4, 0.0), Point(19.4, 16.6), Point(0.0, 16.6)};
        domain.ComputeBoundingBox();
        ReferenceElement rF;
        rF.CreateReferenceElement(poly,poly.boundingBox);
        vector<ReferenceElement> refELements;
        rF.CreateMesh(domain, refELements);
        double toleranceEqualityTest = 1.0E-7;
        double areaDomain = domain.ComputeArea();
        double areaMesh = 0;

        for(unsigned int i = 0; i < refELements.size(); i++)
        {
            if(refELements[i].miniPolygons.size() == 0)
            {
                for(unsigned int j = 0; j < refELements[i].polygons.size(); j++)
                {
                    areaMesh += refELements[i].polygons[j].ComputeArea();
                }
            }
            else
            {
                for(list<Polygon>::iterator it = refELements[i].miniPolygons.begin(); it != refELements[i].miniPolygons.end(); it++)
                {
                    areaMesh += it->ComputeArea();
                }
            }
        }

        try
        {
            EXPECT_TRUE(abs(areaDomain - areaMesh) < toleranceEqualityTest);
        }
        catch (const exception& exception)
        {
            FAIL();
        }

        }

    }

#endif // __TEST_POLYGON_H
