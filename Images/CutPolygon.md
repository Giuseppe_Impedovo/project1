@startuml

class CutPolygon{
+list <Point> newPoints
+list <Point> intersectionPointsList
+vector <Point> intersectionPointsVec
+vector <Segment> polygonEdges
+vector <int> polygonVertices
+IIntersector& _intersecatore
+double toleranceIntersection
+list <Polygon> cuttedPolygon
+CutPolygon(IIntersector& intersecatore)
+Cut(const vector<Point>& points, const vector<int>& vertices, const Segment& segment)
+void SavePolygon(const vector<Point>& points, const vector<int>& vertices)
}

@enduml