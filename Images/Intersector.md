@startuml
class Intersector{
+double toleranceParallelism
+double toleranceIntersection
+Type type
+Vector2d resultParametricCoordinates
+Vector2d originFirstSegment
+Vector2d rightHandSide
+Matrix2d matrixTangentVector
+Intersector()
+void SaveSegments(const Segment& cutterSegment, const Segment& polygonEdge)
+void SetCutterSegment(const Vector2d& origin, const Vector2d& end)
+void SetPolygonEdge(const Vector2d& origin, const Vector2d& end)
+bool ComputeIntersection(const Segment& cutterSegment, const Segment& polygonEdge, list <Point>& newPoints, unsigned int& appEdge, list <Point>& intersectionPointsList, int& numPoints)
}
@enduml