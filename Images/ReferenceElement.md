@startuml

class Point
class Segment
class Polygon

class Point{
 +double X
 +double Y
 +int appSegment
 +double paramCoord
 +int numVert
 +bool passed
 +int friendship
 +int enumTypeIntersection
 +Point()
 +Point(const double& x, const double& y)
 +Point& operator=(const Point& point)
 +bool operator==(const Point& point)
}
class Segment{
+Point From
+Point To
+bool intersection
+Segment(const Point& from, const Point& to)
+Segment& operator=(const Segment& segment)
+bool operator==(const Segment& segment)
}
class Polygon{
+list<int> newPolygonVertices
+bool operator==(const Polygon& polygon)
}


@enduml