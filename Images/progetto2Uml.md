@startuml

interface IIntersector
interface ICutPolygon
class Intersector
class CutPolygon

Intersector  --|>IIntersector : implements
CutPolygon --|> ICutPolygon : implements

ICutPolygon <|-- Point : contains
ICutPolygon <|-- Segment : contains
ICutPolygon <|-- Polygon2 : contains
ReferenceElement <|-- Polygon2 : contains
IIntersector  --|> ICutPolygon : depends
ICutPolygon  --|> Polygon2 : depends


class Point{
 +double X
 +double Y
 +int appSegment
 +double paramCoord
 +int numVert
 +bool passed
 +int friendship
 +int enumTypeIntersection
 +Point()
 +Point(const double& x, const double& y)
 +Point& operator=(const Point& point)
 +bool operator==(const Point& point)
}
class Segment{
+Point From
+Point To
+bool intersection
+Segment(const Point& from, const Point& to)
+Segment& operator=(const Segment& segment)
+bool operator==(const Segment& segment)
}
class Polygon{
+list<int> newPolygonVertices
+list<Point> verticesList
+list<Point> newPolygonPoints
+vector<Point> pPoints
+vector<Point> boundingBox
+ICutPolygon& cutPolygon
+vector<int> verticesVec
+bool operator==(const Polygon& polygon)
+Polygon()
+Polygon(ICutPolygon& cutPolygon)
+double ComputeArea();
+bool PointInPolygon(vector<Point>& points)
+void Traslate(const double& x,const double& y)
+void ComputeBoundingBox()
}
interface IIntersector{
+void SaveSegments(const Segment& cutterSegment, const Segment& polygonEdge)
+void SetCutterSegment(const Vector2d& origin, const Vector2d& end)
+void SetPolygonEdge(const Vector2d& origin, const Vector2d& end)
+bool ComputeIntersection(const Segment& cutterSegment, const Segment& polygonEdge, list <Point>& newPoints, unsigned int& appEdge, list <Point>& intersectionPointsList, int& numPoints)
}
interface ICutPolygon{
+Cut(const vector<Point>& points, const vector<int>& vertices, const Segment& segment)
+void SavePolygon(const vector<Point>& points, const vector<int>& vertices)
}
class ReferenceElement
{
+vector<Polygon> polygons
+vector<Point> boundingBox
+list<Point> addPoints
+list<Polygon> miniPolygons
+ReferenceElement()
+ReferenceElement Traslate(const double& x,const double& y)
+void CreateReferenceElement(Polygon& poly, vector<Point> bBox)
+vector<ReferenceElement>CreateMesh(Polygon& domain, vector<ReferenceElement>& cells)
}


@enduml

