@startuml

interface IIntersector
interface ICutPolygon
class Intersector
class CutPolygon

Intersector  --|>IIntersector : implements
CutPolygon --|> ICutPolygon : implements

ICutPolygon <|-- Point : contains
ICutPolygon <|-- Segment : contains
ICutPolygon <|-- Polygon : contains
IIntersector  --|> ICutPolygon : depends


class Point{
 +double X
 +double Y
 +int appSegment
 +double paramCoord
 +int numVert
 +bool passed
 +int friendship
 +int enumTypeIntersection
 +Point()
 +Point(const double& x, const double& y)
 +Point& operator=(const Point& point)
 +bool operator==(const Point& point)
}
class Segment{
+Point From
+Point To
+bool intersection
+Segment(const Point& from, const Point& to)
+Segment& operator=(const Segment& segment)
+bool operator==(const Segment& segment)
}
class Polygon{
+list<int> newPolygonVertices
+bool operator==(const Polygon& polygon)
}
interface IIntersector{
+void SaveSegments(const Segment& cutterSegment, const Segment& polygonEdge)
+void SetCutterSegment(const Vector2d& origin, const Vector2d& end)
+void SetPolygonEdge(const Vector2d& origin, const Vector2d& end)
+bool ComputeIntersection(const Segment& cutterSegment, const Segment& polygonEdge, list <Point>& newPoints, unsigned int& appEdge, list <Point>& intersectionPointsList, int& numPoints)
}
interface ICutPolygon{
+Cut(const vector<Point>& points, const vector<int>& vertices, const Segment& segment)
+void SavePolygon(const vector<Point>& points, const vector<int>& vertices)
}
@enduml