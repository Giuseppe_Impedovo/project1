# Project 1 - Polygon Cut

Nell'ambito del Calcolo Scientifico un importante applicazione è il taglio di un poligono in diverse parti.

## Requisiti del codice

L'idea del programma Polygon Cut è implementare una funzione *Cut()* con i seguenti requisiti:

* la funzione prende in input:
    * `points`: una collezione di punti nello spazio 2D
    * `polygonVertices`: una collezione di vertici da accoppiare ad ogni punto in modo da creare un poligono 2D ordinato in senso antiorario 
    * `segment`: un segmento 2D che servirà per effettuare il taglio
* restituisce come output:
    * `newPoints`: una collezione di punti creata concatenando i nuovi punti creati con il taglio con la collezione di punti iniziale
    * `cuttedPolygons`: gli N poligoni formati dal taglio come una collezione di vertici ordinati indicizzati in senso antiorario
	
### Esempio

![example](Images/example.png)

Un esempio di come potrebbe una chiamata di funzione in questo caso potrebbe essere:

```Python
points = { {1.5, 1.0}; {5.6, 1.5}; {5.5, 4.8}; {4.0, 6.2}; {3.2, 4.2}; {1.0, 4.0} }
polygonVertices = { 0, 1, 2, 3, 4, 5 }
segment = { {2.0, 3.7}; {4.1, 5.9} }

[newPoints, cuttedPolygons] = CutPolygon(points, polygonVertices, segment)
```
Il poligono risultante dovrebbe essere quindi come il seguente:

![example_result](Images/example_result.png)

```Python
newPoints =  { {1.5, 1.0}; {5.6, 1.5}; {5.5, 4.8}; {4.0, 6.2}; {3.2, 4.2}; {1.0, 4.0}; ... new points... }
cuttedPolygons = { {0, 1, 2, 6, 7, 8, 4, 9, 10, 11}; {6, 3, 8, 7}; {9, 5, 11, 10};  }
```
## Class Diagram: UML

![project1Uml](Images/project1Uml.png)

### Design Pattern: Dependency Injection (CI)

* Vantaggi:
    * l'utilizzo della Constructor Injection ci ha permesso di evitare di utilizzare la classe Injector, in quanto la dipendenza della classe CutPolygon dalla classe Intersector viene iniettata direttamente nel costruttore
    * separare la logica nell'implementazione delle diverse classi
	* testare la nostra funzione Cut slegandola dalla funzione ComputeIntersection utilizzando una Mock Class per l'interfaccia IIntersector
### Classi:

#### - Point, Segment, Polygon

![baseClass](Images/baseClass.png)

Sono le classi raggruppate nel nostro file baseClass e costituiscono la base del poligono.

#### - Intersector

![Intersector](Images/Intersector.png)

E' la classe che permette di calcolare un'eventuale intersezione tra il segmento e i lati del nostro poligono.

#### - CutPolygon

![CutPolygon](Images/CutPolygon.png)

E' la classe che si occupa del taglio del poligono.

## Test: 

Nella fase di testing ci siamo occupati di testare e controllare tutte le funzioni accessorie, ma sopratutto:

* *ComputeIntersection()* con i suoi diversi casi:
    
    * NoIntersection = 0:
    
    ![test1](Images/test5.png)
    
    * IntersectionOnLine = 1:
    
    ![test1](Images/test4.png)
    
    * IntersectionOnSegment = 2:
    
    ![test1](Images/test3.png)
    
    * IntersectionParallelOnLine = 3:
    
    ![test1](Images/test2.png)
    
    * IntersectionParallelOnSegment = 4:
    
    ![test1](Images/test1.png)
    


* *Cut()* su 5 poligoni che presentassero caratteristiche e tipi di intersezione differenti:
    
    
    * 1° poligono
    
    
    ![testCut1](Images/testCut1.png)
    
    ```Python
    newPoints =  { {1.0, 1.0}; {5.0, 1.0}; {5.0, 3.1}; {1.0, 3.1}; {2.0, 1.2}; {4.0, 3.0}; {1.78, 1.0}; {4.11, 3.1} }
    cuttedPolygons = { {0, 1, 2, 6, 7, 8, 4, 9, 10, 11}; {6, 3, 8, 7} }
    ```


    
    * 2° poligono
     
    
    ![testCut2](Images/testCut2.png)
    
    ```Python
    newPoints =  { {2.5, 1.0}; {4.0, 2.1}; {3.4, 4.2}; {1.6, 4.2}; {1.0, 2.1}; {1.2, 2.8}; {1.4, 2.75}; {3.6, 2.2} }
    cuttedPolygons = { {0, 1, 7, 6, 5, 4}; {1, 2, 3, 5, 6, 7}  }
    ```


    
    
    * 3° poligono
     
    
    ![testCut3](Images/testCut3.png)
    
    ```Python
    newPoints =  {{1.5, 1.0}; {5.6, 1.5}; {5.5, 4.8}; {4.0, 6.2}; {3.2, 4.2}; {1.0, 4.0}; {4.28, 6.09}; {3.72, 5.5}; {2.41, 4.13}; {1.19, 2.85}; {2.0, 3.7}; {4.1, 5.9} }
    cuttedPolygons = { {0, 1, 2, 6, 11, 7, 4, 8, 10, 9}; {3, 7, 11, 6}; {5, 9, 10, 8} }
    ```



    * 4° poligono
     
    ![testCut4](Images/testCut4.png)    
    ```Python
    newPoints =  { {0.0, -2.0}; {2.0, 0.0}; {0.0, 2.0}; {-2.0, 0.0} } }
    cuttedPolygons = { {0,1,2,3}  }
    ```
    



    
    * 5° poligono
     
    
    
    ![testCut5](Images/testCut5.png)
    ```Python
    newPoints =  { {2.0, -2.0}; {0.0, -1.0}; {3.0, 1.0}; {0.0, 2.0}; {3.0, 2.0}; {3.0, 3.0}; {-1.0, 3.0}; {-3.0, 1.0}; {0.0, 0.0}; {-3.0, -2.0}; {1.5, 1.5}; {2.0, 2.0}; {-2.0, -2.0} }
    cuttedPolygons = { {0, 1, 2, 10, 8, 12}; {3, 11, 5, 6, 7, 8, 10}; {4, 5, 11}; {9, 12, 8} }
    ```
    
# Project 2 - Create Mesh

Un'altra importante applicazione del calcolo scientifico strettamente collegata con il taglio dei poligoni è la creazione di mesh poligonali.

## Requisiti del codice

L'idea di questa seconda parte del programma è in particolare implementare una funzione *CreateMesh()* con i seguenti requisiti:

* la funzione prende in input:
    * `domain`: un poligono da riempire con celle di "ReferenceElement" 
    * `cells`: un vettore di "ReferenceElement" inizialmente vuoto
* restituisce come output:
    * `cells`: il vettore passato in input contenente tutte le celle e che costituisce la nostra mesh
    
## Class Diagram: UML

![progetto2Uml](Images/progetto2Uml.png)

### Nuove classi:

#### - Polygon

![polygon2](Images/polygon2.png)

La classe Polygon acquisisce nuovi attributi e una nuova logica, in quanto è ora indispensabile salvare il poligono oltre che come collezione di vertici, anche come collezione di punti.

In particolare anche la logica del taglio del poligono è adesso spostata in questa classe, la quale dipende ora infatti dalla classe CutPolygon. Dipendenza iniettata direttamente nel costruttore sempre con la Constructor Injection.

#### - ReferenceElement

![ReferenceElement](Images/ReferenceElement.png)

Il Reference Element citato sopra è l'elemento di riferimento che ricoprirà la nostra mesh, il quale è costituito da un poligono principale circondato da una bounding box.

Il poligono più la bounding box daranno vita anche ad altri poligoni, che insieme al poligono principale e alla bounding box costituiscono il Reference Element.



## Test: 

La fase di testing per la seconda parte, come da requirements è mirata solamente al verificare che il taglio delle celle che non rientrano completamente nel dominio sia stata effettuato correttamente.

In particolare è stato quindi necessario implementare per la classe Polygon il metodo ComputeArea(), e testare che la somma dell'area di tutti i poligoni da cui è costituita la mesh sia uguale, a meno di piccola tolleranza, all'area del dominio dato in input.

* Sono presenti dunque due test:
    
    * Test di *ComputeArea()*: Test per verificare la corretta implementazione del metodo dei triangoli per il calcolo dell'area.
    
    
    * Test di *CreatMesh()*: Il test che, come specificato sopra verifica che la somma dell'area di tutti i poligoni da cui è costituita la mesh sia uguale all'area del dominio dato in input.


