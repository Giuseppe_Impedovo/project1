class Point:
    def __init__(self, x: float, y: float):
        self.X = x
        self.Y = y
        self.appSegment = -2
        self.paramCoord = 0
        self.numVert = -1
        self.passed = None
        self.friendship = None
        self.enumTypeIntersection = None

    def __eq__(self, other):
        self: Point
        other: Point
        return (abs(self.X - other.X) < 10E-7 ) and (abs(self.Y - other.Y < 10E-7))

class Segment:
    def __init__(self, _from: Point, _to: Point):
        self.From = _from
        self.To = _to
        self.intersection = None

    def __eq__(self, other):
        return self.From == other.From and self.To == other.To

class Polygon:
    def __init__(self):
        self.newPolygonVertices = []

    def __eq__(self, other) :
        return self.newPolygonVertices == other.newPolygonVertices

class ICutPolygon:
    def SavePolygon(self,
                    points: [],
                    vertices: []) -> None:
        pass

    def Cut(self,
            points: [],
            vertices: [],
            segment: Segment) -> None:
        pass

class IIntersector:
    def SaveSegments(self,
                     cutterSegment: Segment,
                     polygonEdge: Segment) -> None:
        pass

    def SetCutterSegment(self,
                         origin: (),  # vector2d
                         end: ()) -> None:  # vector2d
        pass

    def SetPolygonEdge(self,
                       origin: (),  # vector2d
                       end: ()) -> None:  # vector2d
        pass

    def ComputeIntersection(self,
                            cutterSegment: Segment,
                            polygonEdge: Segment,
                            newPoints: [],
                            appEdge: int,
                            intersectionPointList: [],
                            numPoints: int) -> (bool, list, list):
        pass