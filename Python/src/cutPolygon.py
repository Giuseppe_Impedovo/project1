import src.baseClass as baseClass
from src.baseClass import Point, Segment, Polygon, ICutPolygon, IIntersector

class CutPolygon(ICutPolygon):
    def __init__(self, intersecatore: IIntersector):
        self.newPoints = []
        self.intersectionPointsList = []
        self.intersectionPointsVec = []
        self.polygonEdges = []
        self.polygonVertices = []
        self.intersecatore = intersecatore
        self.toleranceIntersection = 1.0E-7
        self.cuttedPolygon = []
        pass

    def SavePolygon(self,
                    points: [],
                    vertices: []) -> None:
        numPoints = len(points)
        for i in range(0, numPoints):
            self.newPoints.append(points[i])

        i = 0
        newpoint: Point
        for newpoint in self.newPoints:
            newpoint.numVert = i
            i = i+1
        self.polygonEdges = [] * (numPoints - 1)
        for j in range(0, numPoints):
            if j == numPoints - 1 and numPoints > 2:
                tmpSegment = Segment(points[j], points[0])
                tmpSegment.From.passed = False
                tmpSegment.From.numVert = j
                self.polygonEdges.append(tmpSegment)
            elif j != numPoints - 1:
                tmpSegment = Segment(points[j], points[j+1])
                tmpSegment.From.passed = False
                tmpSegment.From.numVert = j
                self.polygonEdges.append(tmpSegment)

        return None

    def Cut(self,
            points: [],
            vertices: [],
            segment: Segment) -> None:

        self.SavePolygon(points, vertices)
        for i in range(0, len(self.polygonEdges)):
            numPoints: int = len(self.newPoints)
            [self.polygonEdges[i].intersection, self.newPoints, self.intersectionPointsList] = self.intersecatore.ComputeIntersection(segment, self.polygonEdges[i], self.newPoints, i, self.intersectionPointsList, numPoints)
        numPoints = len(self.newPoints)
        flagFrom = 1
        flagTo = 1
        for newpoint in self.intersectionPointsList:
            if newpoint.paramCoord < self.toleranceIntersection and flagFrom == 1:
                flagFrom = 0
            if newpoint.paramCoord > 1 - self.toleranceIntersection and flagTo == 1:
                flagTo = 0
        if flagFrom == 0:
            segFromPoint = Point(segment.From.X, segment.From.Y)
            segFromPoint.paramCoord = 0.0
            segFromPoint.appSegment = -1
            segFromPoint.numVert = numPoints
            numPoints = numPoints + 1
            self.newPoints.append(segFromPoint)
            self.intersectionPointsList.append(segFromPoint)
        if flagTo == 0:
            segFromPoint = Point(segment.To.X, segment.To.Y)
            segFromPoint.paramCoord = 1
            segFromPoint.appSegment = -1
            segFromPoint.numVert = numPoints
            numPoints = numPoints + 1
            self.newPoints.append(segFromPoint)
            self.intersectionPointsList.append(segFromPoint)

        self.intersectionPointsVec = [Point(0, 0)] * len(self.intersectionPointsList)
        i = 0
        for newpoint in self.intersectionPointsList:
            flag3 = 1
            self.intersectionPointsVec[i] = newpoint
            for j in range(i, 0, -1):
                if flag3 == 1:
                    if self.intersectionPointsVec[j].paramCoord < self.intersectionPointsVec[j - 1].paramCoord:
                        tmpPoint = self.intersectionPointsVec[j]
                        self.intersectionPointsVec[j] = self.intersectionPointsVec[j - 1]
                        self.intersectionPointsVec[j - 1] = tmpPoint
                    else:
                        flag3 = 0
            i = i + 1

        for i in range(1, len(self.intersectionPointsVec)):
            if self.intersectionPointsVec[i].numVert == self.intersectionPointsVec[i - 1].numVert:
                indice = self.intersectionPointsVec[i].appSegment - 1
                controllo = (segment.To.X - segment.From.X)*(self.polygonEdges[indice].From.Y - segment.From.Y) - (segment.To.Y - segment.From.Y)*(self.polygonEdges[indice].From.X - segment.From.X)
                if controllo > self.toleranceIntersection:
                    tmpPoint = self.intersectionPointsVec[i]
                    self.intersectionPointsVec[i] = self.intersectionPointsVec[i - 1]
                    self.intersectionPointsVec[i - 1] = tmpPoint

        flag4 = 0
        friendship = 0
        for i in range(0, len(self.intersectionPointsVec)):
            if self.intersectionPointsVec[i].appSegment != -1:
                if i == 0 and self.intersectionPointsVec[i].numVert == self.intersectionPointsVec[i + 1].numVert:
                    self.intersectionPointsVec[i].friendship = friendship
                    friendship = friendship + 1
                else:
                    self.intersectionPointsVec[i].friendship = friendship
                    if flag4 < 1:
                        flag4 = flag4 + 1
                    else:
                        flag4 = flag4 - 1
                        friendship = friendship + 1
            else:
                self.intersectionPointsVec[i].friendship = -1

        self.cuttedPolygon.append(Polygon())
        arrival = Point(0, 0)
        flag5 = 0
        flagFine = 0
        s = 0
        h = 0
        while s < len(self.polygonEdges) and flagFine != len(points):
            if self.polygonEdges[s].From.passed == False:
                self.polygonEdges[s].From.passed = True
                flagFine = flagFine + 1
                self.cuttedPolygon[h].newPolygonVertices.append(self.polygonEdges[s].From.numVert)
                if self.polygonEdges[s].intersection:
                    flag5 = 0
                    for i in range(0, len(self.intersectionPointsVec)):
                        if flag5 == 0:
                            if s == self.intersectionPointsVec[i].appSegment:
                                flag5 = 1
                                if self.intersectionPointsVec[i].enumTypeIntersection != 0:
                                    self.cuttedPolygon[h].newPolygonVertices.append(self.intersectionPointsVec[i].numVert)
                                if i + 1 < len(self.intersectionPointsVec) and self.intersectionPointsVec[i + 1].friendship == self.intersectionPointsVec[i].friendship:
                                    arrival = self.intersectionPointsVec[i + 1]
                                    s = self.intersectionPointsVec[i + 1].appSegment
                                elif i + 1 < len(self.intersectionPointsVec) and self.intersectionPointsVec[i + 1].friendship == -1 and self.intersectionPointsVec[i + 2].friendship == self.intersectionPointsVec[i].friendship:
                                    self.cuttedPolygon[h].newPolygonVertices.append(self.intersectionPointsVec[i + 1].numVert)
                                    arrival = self.intersectionPointsVec[i + 2]
                                    s = self.intersectionPointsVec[i + 2].appSegment
                                elif i + 1 < len(self.intersectionPointsVec) and self.intersectionPointsVec[i + 1].friendship == -1 and self.intersectionPointsVec[i + 2].friendship == -1 and self.intersectionPointsVec[i + 3].friendship == self.intersectionPointsVec[i].friendship:
                                    self.cuttedPolygon[h].newPolygonVertices.append(self.intersectionPointsVec[i + 1].numVert)
                                    self.cuttedPolygon[h].newPolygonVertices.append(self.intersectionPointsVec[i + 2].numVert)
                                    arrival = self.intersectionPointsVec[i + 3]
                                    s = self.intersectionPointsVec[i + 3].appSegment
                                elif self.intersectionPointsVec[i - 1].friendship == self.intersectionPointsVec[i].friendship:
                                    arrival = self.intersectionPointsVec[i - 1]
                                    s = self.intersectionPointsVec[i - 1].appSegment
                                elif self.intersectionPointsVec[i - 1].friendship == -1 and self.intersectionPointsVec[i - 2].friendship == self.intersectionPointsVec[i].friendship:
                                    self.cuttedPolygon[h].newPolygonVertices.append(self.intersectionPointsVec[i - 1].numVert)
                                    arrival = self.intersectionPointsVec[i - 2]
                                    s = self.intersectionPointsVec[i - 2].appSegment
                                elif self.intersectionPointsVec[i - 1].friendship == -1 and self.intersectionPointsVec[i - 2].friendship == -1 and self.intersectionPointsVec[i - 3].friendship == self.intersectionPointsVec[i].friendship:
                                    self.cuttedPolygon[h].newPolygonVertices.append(self.intersectionPointsVec[i - 1].numVert)
                                    self.cuttedPolygon[h].newPolygonVertices.append(self.intersectionPointsVec[i - 2].numVert)
                                    arrival = self.intersectionPointsVec[i - 3]
                                    s = self.intersectionPointsVec[i - 3].appSegment
                                if arrival.enumTypeIntersection != 2 and s == arrival.appSegment:
                                    self.cuttedPolygon[h].newPolygonVertices.append(arrival.numVert)

                if s == len(self.polygonEdges) - 1 and flagFine != len(points):
                    s = -1
            elif self.polygonEdges[s].From.passed and len(self.cuttedPolygon[h].newPolygonVertices) > 2:
                self.cuttedPolygon.append(Polygon())
                h = h + 1
                s = 0
            s = s + 1
        if len(self.cuttedPolygon[h].newPolygonVertices) < 3:
            self.cuttedPolygon.pop()
        return None