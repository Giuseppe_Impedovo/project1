from src.baseClass import Point, Segment, IIntersector
import numpy as np

class Intersector(IIntersector):
    def __init__(self):
        self.toleranceParallelism = 1.0e-7
        self.toleranceIntersection = 1.0e-7
        self.type = 0  # 0 = NO, 1 = IntersectionOnLine, 2 = IntersectionOnSegment, 3 = IntersectionParallelOnLine, 4 = IntersectionParallelOnSegment
        self.resultParametricCoordinates = np.zeros((2, 1))  # vector2d
        self.originFirstSegment = np.zeros((2, 1))  # vector2d
        self.rightHandSide = np.zeros((2, 1))  # vector2d
        self.matrixTangentVector = np.zeros((2, 2))  # matrix2d
        pass


    def SetCutterSegment(self,
                         origin: (),  # vector2d
                         end: ()) -> None:

        self.matrixTangentVector[:, 0] = end - origin
        self.originFirstSegment = origin

        return None

    def SetPolygonEdge(self,
                       origin: (),  # vector2d
                       end: ()) -> None:
        self.matrixTangentVector[:, 1] = origin - end
        self.rightHandSide = origin - self.originFirstSegment

        return None

    def SaveSegments(self,
                     cutterSegment: Segment,
                     polygonEdge: Segment) -> None:
        b = np.array([[5, 6]])
        originCutterVector = np.array([[cutterSegment.From.X, cutterSegment.From.Y]])
        endCutterVector = np.array([[cutterSegment.To.X, cutterSegment.To.Y]])
        originPolygonVector = np.array([[polygonEdge.From.X, polygonEdge.From.Y]])
        endPolygonVector = np.array([[polygonEdge.To.X, polygonEdge.To.Y]])
        self.SetCutterSegment(originCutterVector, endCutterVector)
        self.SetPolygonEdge(originPolygonVector, endPolygonVector)

        return None


    def ComputeIntersection(self,
                            cutterSegment: Segment,
                            polygonEdge: Segment,
                            newPoints: [],
                            appEdge: int,
                            intersectionPointsList: [],
                            numPoints: int) -> (bool, list, list):

        self.SaveSegments(cutterSegment, polygonEdge)
        determinant = np.linalg.det(self.matrixTangentVector)
        self.type = 0
        polygonEdge.intersection = False
        self.type = 0
        check = self.toleranceParallelism * self.toleranceParallelism * np.linalg.norm(self.matrixTangentVector[:, 0]) * np.linalg.norm(self.matrixTangentVector[:, 1]) * np.linalg.norm(self.matrixTangentVector[:, 0]) * np.linalg.norm(self.matrixTangentVector[:, 1])

        if determinant * determinant >= check:
            solverMatrix = np.zeros((2, 2))
            solverMatrix[0, 0] = self.matrixTangentVector[1, 1] / determinant
            solverMatrix[0, 1] = - self.matrixTangentVector[0, 1] / determinant
            solverMatrix[1, 0] = - self.matrixTangentVector[1, 0] / determinant
            solverMatrix[1, 1] = self.matrixTangentVector[0, 0] / determinant
            self.resultParametricCoordinates = solverMatrix.dot(self.rightHandSide.T)
            if - self.toleranceIntersection < self.resultParametricCoordinates[1, 0] < 1 + self.toleranceIntersection:
                self.type = 1
                polygonEdge.intersection = True
                if - self.toleranceIntersection < self.resultParametricCoordinates[0, 0] < 1 + self.toleranceIntersection:
                    self.type = 2
        else:
            determinant2 = abs(self.matrixTangentVector[0, 0] * self.rightHandSide[0, 1] - self.rightHandSide[0, 0] * self.matrixTangentVector[1, 0])
            check2 = self.toleranceParallelism * self.toleranceParallelism * np.linalg.norm(self.matrixTangentVector[:, 0]) * np.linalg.norm(self.rightHandSide)
            if determinant2 * determinant2 <= check2:
                a = np.linalg.norm(self.matrixTangentVector[:, 0]) * np.linalg.norm(self.matrixTangentVector[:, 0])
                tempNorm = 1.0 / a
                self.resultParametricCoordinates[0, 0] = (self.matrixTangentVector[:, 0].T.dot(self.rightHandSide.T)) * tempNorm
                self.resultParametricCoordinates[1, 0] = self.resultParametricCoordinates[0, 0] - (self.matrixTangentVector[:, 0].T.dot(self.matrixTangentVector[:, 1])) * tempNorm
                polygonEdge.intersection = True
                self.type = 3
                if self.resultParametricCoordinates[1, 0] < self.resultParametricCoordinates[0, 0]:
                    tmp = self.resultParametricCoordinates[0, 0]
                    self.resultParametricCoordinates[0, 0] = self.resultParametricCoordinates[1, 0]
                    self.resultParametricCoordinates[1, 0] = tmp

                if (-self.toleranceIntersection < self.resultParametricCoordinates[0, 0] < 1.0 + self.toleranceIntersection) or (-self.toleranceIntersection < self.resultParametricCoordinates[1, 0] < 1.0 + self.toleranceIntersection):
                    self.type = 4
                else:
                    if self.resultParametricCoordinates[1, 0] < self.toleranceIntersection and self.resultParametricCoordinates[1, 0] - 1.0 > -self.toleranceIntersection:
                        self.type = 4
        if self.type != 3 and self.type != 4:
            if self.type != 0:
                tmpPoint = Point((self.rightHandSide[0, 0] + self.originFirstSegment[0, 0]) + self.resultParametricCoordinates[1, 0] * (-self.matrixTangentVector[0, 1]), (self.rightHandSide[0, 1] + self.originFirstSegment[0, 1]) + self.resultParametricCoordinates[1, 0] * (-self.matrixTangentVector[1, 1]))
                tmpPoint.appSegment = appEdge
                tmpPoint.paramCoord = self.resultParametricCoordinates[0, 0]
                if self.resultParametricCoordinates[1, 0] > -self.toleranceIntersection and self.resultParametricCoordinates[1, 0] < self.toleranceIntersection:
                    tmpPoint.enumTypeIntersection = 0
                    tmpPoint.numVert = polygonEdge.From.numVert
                    intersectionPointsList.append(tmpPoint)
                elif 1.0 - self.toleranceIntersection < self.resultParametricCoordinates[1, 0] < 1.0 + self.toleranceIntersection:
                    tmpPoint.numVert = polygonEdge.To.numVert
                    tmpPoint.enumTypeIntersection = 2
                    intersectionPointsList.append(tmpPoint)
                else:
                    tmpPoint.numVert = numPoints
                    tmpPoint.enumTypeIntersection = 1
                    newPoints.append(tmpPoint)
                    intersectionPointsList.append(tmpPoint)


        return polygonEdge.intersection, newPoints, intersectionPointsList

