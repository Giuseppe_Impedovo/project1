from unittest import TestCase, mock
import src.intersector as intersector
import src.cutPolygon as cutpolygon
import src.baseClass as baseClass
from src.baseClass import Point, Segment, Polygon, ICutPolygon, IIntersector

class TestCutPolygon(TestCase):

    def test_SavePolygon(self):
        intersecatore = intersector.Intersector()
        cutPolygon = cutpolygon.CutPolygon(intersecatore)
        points = []
        A = Point(1.0, 1.0)
        B = Point(3.0, 2.5)
        points.append(A)
        points.append(B)
        controlList = [A, B]
        controlSeg = Segment(A, B)
        controlVec = [controlSeg]
        vertices = [0, 1]

        try:
            cutPolygon.SavePolygon(points, vertices)
            self.assertEqual(cutPolygon.newPoints, controlList)
            self.assertEqual(cutPolygon.polygonEdges, controlVec)

        except Exception as ex:
            self.fail()

    def test_Cut1(self):
        intersecatore_mock = mock.Mock(spec=baseClass.IIntersector)
        cutPolygon = cutpolygon.CutPolygon(intersecatore_mock)

        A = Point(1.0, 1.0)
        B = Point(5.0, 1.0)
        C = Point(5.0, 3.1)
        D = Point(1.0, 3.1)
        E = Point(2.0, 1.2)
        F = Point(4.0, 3.0)
        inters1 = Point(1.8, 1.0)
        inters1.numVert = 4
        inters1.enumTypeIntersection = 1
        inters1.appSegment = 0
        inters1.paramCoord = -0.11111111
        inters2 = Point(4.1, 3.1)
        inters2.numVert = 5
        inters2.enumTypeIntersection = 1
        inters2.appSegment = 2
        inters2.paramCoord = 1.0555556
        initialPoints = [A, B, C, D]
        initialPointsList = [Point]
        poligono1 = baseClass.Polygon()
        poligono1.newPolygonVertices = [0, 4, 6, 7, 5, 3]
        poligono2 = baseClass.Polygon()
        poligono2.newPolygonVertices = [1, 2, 5, 7, 6, 4]
        finalCuttedPolygons = [poligono1, poligono2]
        newPoints1 = [A, B, C, D, inters1]
        newPoints2 = [A, B, C, D, inters1, inters2]
        intPoints1 = [inters1]
        intPoints2 = [inters1, inters2]
        finalPoints = [A, B, C, D, inters1, inters2, E, F]
        initialSegment = Segment(E, F)
        initialVertices = [0, 1, 2, 3]

        intersecatore_mock.ComputeIntersection.side_effect = [[True, newPoints1, intPoints1], [False, newPoints1, intPoints1], [True, newPoints2, intPoints2], [False, newPoints2, intPoints2]]

        try:
            cutPolygon.Cut(initialPoints, initialVertices, initialSegment)
            self.assertEqual(cutPolygon.newPoints, finalPoints)
            self.assertEqual(cutPolygon.cuttedPolygon, finalCuttedPolygons)

        except Exception as ex:
            self.fail()


    def test_Cut2(self):
        intersecatore_mock = mock.Mock(spec=baseClass.IIntersector)
        cutPolygon = cutpolygon.CutPolygon(intersecatore_mock)

        A = Point(2.5, 1.0)
        B = Point(4.0, 2.1)
        C = Point(3.4, 4.2)
        D = Point(1.6, 4.2)
        E = Point(1.0, 2.1)
        F = Point(1.4, 2.75)
        G = Point(3.6, 2.2)
        initialPoints = [A, B, C, D, E]
        inters1 = Point(4.0, 2.1)
        inters1.numVert = 1
        inters1.enumTypeIntersection = 2
        inters1.appSegment = 0
        inters1.paramCoord = 1.18182
        inters2 = Point(4.0, 2.1)
        inters2.numVert = 1
        inters2.enumTypeIntersection = 0
        inters2.appSegment = 1
        inters2.paramCoord = 1.18182
        inters3 = Point(1.2, 2.8)
        inters3.numVert = 5
        inters3.enumTypeIntersection = 1
        inters3.appSegment = 3
        inters3.paramCoord = -0.090909094
        initialSegment = Segment(F, G)
        initialVertices = {0, 1, 2, 3, 4}
        intPoints1 = [inters1]
        intPoints2 = [inters1, inters2]
        intPoints3 = [inters1, inters2, inters3]
        newPoints1 = [A, B, C, D, E, inters3]
        finalPoints = [A, B, C, D, E, inters3, F, G]
        poligono1 = Polygon()
        poligono1.newPolygonVertices = [0, 1, 7, 6, 5, 4]
        poligono2 = Polygon()
        poligono2.newPolygonVertices = [1, 2, 3, 5, 6, 7]
        finalCuttedPolygons = [poligono1, poligono2]

        intersecatore_mock.ComputeIntersection.side_effect = [[True, initialPoints, intPoints1], [False, initialPoints, intPoints2], [False, initialPoints, intPoints2], [True, newPoints1, intPoints3], [False, newPoints1, intPoints3]]

        try:
            cutPolygon.Cut(initialPoints, initialVertices, initialSegment)
            self.assertEqual(cutPolygon.newPoints, finalPoints)
            self.assertEqual(cutPolygon.cuttedPolygon, finalCuttedPolygons)

        except Exception as ex:
            self.fail()

    def test_Cut3(self):
        intersecatore_mock = mock.Mock(spec=baseClass.IIntersector)
        cutPolygon = cutpolygon.CutPolygon(intersecatore_mock)

        A = Point(1.5, 1.0)
        B = Point(5.6, 1.5)
        C = Point(5.5, 4.8)
        D = Point(4.0, 6.2)
        E = Point(3.2, 4.2)
        F = Point(1.0, 4.0)
        G = Point(2.0, 3.7)
        H = Point(4.1, 5.9)
        initialPoints = [A, B, C, D, E, F]
        inIntPoint = []
        initialSegment = Segment(G, H)
        initialVertices = [0, 1, 2, 3, 4, 5]
        inters1 = Point(4.204326923, 6.0092948718)
        inters1.numVert = 6
        inters1.enumTypeIntersection = 1
        inters1.appSegment = 2
        inters1.paramCoord = 1.0496795
        inters2 = Point(3.721311475409, 5.5032786885)
        inters2.numVert = 7
        inters2.enumTypeIntersection = 1
        inters2.appSegment = 3
        inters2.paramCoord = 0.81967211
        inters3 = Point(2.408597285067, 4.12805429864)
        inters3.numVert = 8
        inters3.enumTypeIntersection = 1
        inters3.appSegment = 4
        inters3.paramCoord = 0.19457014
        inters4 = Point(1.1912162162162, 2.85270270270)
        inters4.numVert = 9
        inters4.enumTypeIntersection = 1
        inters4.appSegment = 5
        inters4.paramCoord = -0.38513514
        newPoints1 = [A, B, C, D, E, F, inters1]
        intPoints1 = [inters1]
        newPoints2 = [A, B, C, D, E, F, inters1, inters2]
        intPoints2 = [inters1, inters2]
        newPoints3 = [A, B, C, D, E, F, inters1, inters2, inters3]
        intPoints3 = [inters1, inters2, inters3]
        newPoints4 = [A, B, C, D, E, F, inters1, inters2, inters3, inters4]
        intPoints4 = [inters1, inters2, inters3, inters4]
        finalPoints = [A, B, C, D, E, F, inters1, inters2, inters3, inters4, G, H]
        poligono1 = Polygon()
        poligono1.newPolygonVertices = [0, 1, 2, 6, 11, 7, 4, 8, 10, 9]
        poligono2 = Polygon()
        poligono2.newPolygonVertices = [3, 7, 11, 6]
        poligono3 = Polygon()
        poligono3.newPolygonVertices = [5, 9, 10, 8]
        finalCuttedPolygons = [poligono1, poligono2, poligono3]
        i = 0
        numP = 6

        intersecatore_mock.ComputeIntersection.side_effect = [[False, initialPoints, inIntPoint], [False, initialPoints, inIntPoint], [True, newPoints1, intPoints1], [True, newPoints2, intPoints2], [True, newPoints3, intPoints3], [True, newPoints4, intPoints4]]

        try:
            cutPolygon.Cut(initialPoints, initialVertices, initialSegment)
            self.assertEqual(cutPolygon.newPoints, finalPoints)
            self.assertEqual(cutPolygon.cuttedPolygon, finalCuttedPolygons)

        except Exception as ex:
            self.fail()

    def test_Cut4Conv(self):
        intersecatore_mock = mock.Mock(spec=baseClass.IIntersector)
        cutPolygon = cutpolygon.CutPolygon(intersecatore_mock)

        A = Point(0.0, -2.0)
        B = Point(2.0, 0.0)
        C = Point(0.0, 2.0)
        D = Point(-2.0, 0.0)
        E = Point(-3.0, -1.0)
        F = Point(1.0, 3.0)
        initialPoints = [A, B, C, D]
        inIntPoint = []
        initialSegment = Segment(E, F)
        initialVertices = [0, 1, 2, 3]
        inters1 = Point(0.0, 2.0)
        inters1.numVert = 2
        inters1.enumTypeIntersection = 2
        inters1.appSegment = 1
        inters1.paramCoord = 0.75
        inters2 = Point(-2.0, 0.0)
        inters2.numVert = 3
        inters2.enumTypeIntersection = 0
        inters2.appSegment = 3
        inters2.paramCoord = 0.25
        intPoints1 = [inters1]
        intPoints2 = [inters1, inters2]
        finalPoints = [A, B, C, D]
        poligono1 = Polygon()
        poligono1.newPolygonVertices = [0, 1, 2, 3]
        finalCuttedPolygons = [poligono1]
        i = 0
        numP = 4

        intersecatore_mock.ComputeIntersection.side_effect = [[False, initialPoints, inIntPoint], [True, initialPoints, intPoints1], [True, initialPoints, intPoints1], [True, initialPoints, intPoints2]]

        try:
            cutPolygon.Cut(initialPoints, initialVertices, initialSegment)
            self.assertEqual(cutPolygon.newPoints, finalPoints)
            self.assertEqual(cutPolygon.cuttedPolygon, finalCuttedPolygons)

        except Exception as ex:
            self.fail()

    def test_Cut5Conc(self):
        intersecatore_mock = mock.Mock(spec=baseClass.IIntersector)
        cutPolygon = cutpolygon.CutPolygon(intersecatore_mock)

        A = Point(2.0, -2.0)
        B = Point(0.0, -1.0)
        C = Point(3.0, 1.0)
        D = Point(0.0, 2.0)
        E = Point(3.0, 2.0)
        F = Point(3.0, 3.0)
        G = Point(-1.0, 3.0)
        H = Point(-3.0, 1.0)
        I = Point(0.0, 0.0)
        J = Point(-3.0, -2.0)
        K = Point(-4.0, -4.0)
        L = Point(4.0, 4.0)
        initialPoints = [A, B, C, D, E, F, G, H, I, J]
        inIntPoint  = []
        initialSegment = Segment(K, L)
        initialVertices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        inters1 = Point(1.5, 1.5)
        inters1.numVert = 10
        inters1.enumTypeIntersection = 1
        inters1.appSegment = 2
        inters1.paramCoord = 0.6875
        inters2 = Point(2.0, 2.0)
        inters2.numVert = 11
        inters2.enumTypeIntersection = 1
        inters2.appSegment = 3
        inters2.paramCoord = 0.75
        inters3 = Point(3.0, 3.0)
        inters3.numVert = 5
        inters3.enumTypeIntersection = 2
        inters3.appSegment = 4
        inters3.paramCoord = 0.875
        inters4 = Point(3.0, 3.0)
        inters4.numVert = 5
        inters4.enumTypeIntersection = 0
        inters4.appSegment = 5
        inters4.paramCoord = 0.875
        inters5 = Point(0.0, 0.0)
        inters5.numVert = 8
        inters5.enumTypeIntersection = 2
        inters5.appSegment = 7
        inters5.paramCoord = 0.5
        inters6 = Point(0.0, 0.0)
        inters6.numVert = 8
        inters6.enumTypeIntersection = 0
        inters6.appSegment = 8
        inters6.paramCoord = 0.5
        inters7 = Point(-2.0, -2.0)
        inters7.numVert = 12
        inters7.enumTypeIntersection = 1
        inters7.appSegment = 9
        inters7.paramCoord = 0.25
        i = 0
        numP = 10
        newPoints1 = [A, B, C, D, E, F, G, H, I, J, inters1]
        newPoints2 = [A, B, C, D, E, F, G, H, I, J, inters1, inters2]
        finalPoints = [A, B, C, D, E, F, G, H, I, J, inters1, inters2, inters7]
        intPoints1 = [inters1]
        intPoints2 = [inters1, inters2]
        intPoints3 = [inters1, inters2, inters3]
        intPoints4 = [inters1, inters2, inters3, inters4]
        intPoints5 = [inters1, inters2, inters3, inters4, inters5]
        intPoints6 = [inters1, inters2, inters3, inters4, inters5, inters6]
        intPoints7 = [inters1, inters2, inters3, inters4, inters5, inters6, inters7]
        poligono1 = Polygon()
        poligono1.newPolygonVertices = [0, 1, 2, 10, 8, 12]
        poligono2 = Polygon()
        poligono2.newPolygonVertices = [3, 11, 5, 6, 7, 8, 10]
        poligono3 = Polygon()
        poligono3.newPolygonVertices = [4, 5, 11]
        poligono4 = Polygon()
        poligono4.newPolygonVertices = [9, 12, 8]
        finalCuttedPolygons = [poligono1, poligono2, poligono3, poligono4]

        intersecatore_mock.ComputeIntersection.side_effect = [[False, initialPoints, inIntPoint], [False, initialPoints, inIntPoint], [True, newPoints1, intPoints1], [True, newPoints2, intPoints2], [True, newPoints2, intPoints3], [True, newPoints2, intPoints4], [False, newPoints2, intPoints4], [True, newPoints2, intPoints5], [True, newPoints2, intPoints6], [True, finalPoints, intPoints7]]

        try:
            cutPolygon.Cut(initialPoints, initialVertices, initialSegment)
            self.assertEqual(cutPolygon.newPoints, finalPoints)
            self.assertEqual(cutPolygon.cuttedPolygon, finalCuttedPolygons)

        except Exception as ex:
            self.fail()

