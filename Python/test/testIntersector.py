from unittest import TestCase, mock
from src.baseClass import Point, Segment, Polygon, ICutPolygon, IIntersector
import src.intersector as intersector
import numpy as np

class TestIntersector(TestCase):
    def test_SaveAll(self):
        intersecatore = intersector.Intersector()
        A = Point(1.0, 1.0)
        B = Point(3.0, 1.0)
        C = Point(2.0, 0.0)
        D = Point(2.0, 2.0)
        cSegment = Segment(A, B)
        pSegment = Segment(C, D)
        control1 = np.array([[2, 0]])
        control2 = np.array([[1, 1]])
        control3 = np.array([[0, -2]])
        control4 = np.array([[1, -1]])

        try:
            intersecatore.SaveSegments(cSegment, pSegment)
            self.assertEqual(intersecatore.matrixTangentVector[:,0].any(), control1.any())
            self.assertEqual(intersecatore.originFirstSegment.any(), control2.any())
            self.assertEqual(intersecatore.matrixTangentVector[:,1].any(), control3.any())
            self.assertEqual(intersecatore.rightHandSide.any(), control4.any())

        except Exception as ex:
            self.fail()

    def test_ParallelIntersectionOnSegment(self):
        intersecatore = intersector.Intersector()
        A = Point(0.0, 0.0)
        B = Point(4.0, 0.0)
        C = Point(1.0, 0.0)
        D = Point(2.0, 0.0)
        cSegment = Segment(A, B)
        pSegment = Segment(C, D)
        cNewPoints = []
        controlAppEdge = 0
        controlInterPoints = []

        try:
            intersecatore.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, len(cNewPoints))
            self.assertEqual(pSegment.intersection, True)
            self.assertEqual(intersecatore.type, 4)
            self.assertEqual(0.25 - 10E-7 < intersecatore.resultParametricCoordinates[0, 0] < 0.25 + 10E-7, True)
            self.assertEqual(0.5 - 10E-7 < intersecatore.resultParametricCoordinates[1, 0] < 0.5 + 10E-7, True)

        except Exception as ex:
            self.fail()

    def test_ParallelIntersectionOnLine(self):
        intersecatore = intersector.Intersector()
        A = Point(0.0, 0.0)
        B = Point(4.0, 0.0)
        C = Point(1.0, 0.0)
        D = Point(2.0, 0.0)
        cSegment = Segment(A, C)
        pSegment = Segment(D, B)
        cNewPoints = []
        controlAppEdge = 0
        controlInterPoints = []

        try:
            intersecatore.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, len(cNewPoints))
            self.assertEqual(pSegment.intersection, True)
            self.assertEqual(intersecatore.type, 3)
            self.assertEqual(2 - 10E-7 < intersecatore.resultParametricCoordinates[0, 0] < 2 + 10E-7, True)
            self.assertEqual(4 - 10E-7 < intersecatore.resultParametricCoordinates[1, 0] < 4 + 10E-7, True)

        except Exception as ex:
            self.fail()

    def test_OnSegmentIntersectionInner(self):
        intersecatore = intersector.Intersector()
        A = Point(0.0, 2.0)
        B = Point(0.0, 4.0)
        C = Point(2.0, 3.0)
        D = Point(-2.0, 3.0)
        cSegment = Segment(C, D)
        pSegment = Segment(A, B)
        cNewPoints = []
        controlAppEdge = 0
        controlInterPoints = []

        try:
            intersecatore.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, len(cNewPoints))
            self.assertEqual(pSegment.intersection, True)
            self.assertEqual(intersecatore.type, 2)
            self.assertEqual(0.5 - 10E-7 < intersecatore.resultParametricCoordinates[0, 0] < 0.5 + 10E-7, True)
            self.assertEqual(0.5 - 10E-7 < intersecatore.resultParametricCoordinates[1, 0] < 0.5 + 10E-7, True)
            self.assertEqual(len(cNewPoints), 1)
            self.assertEqual(- 10E-7 < controlInterPoints[0].X < 10E-7, True)
            self.assertEqual(3 - 10E-7 < controlInterPoints[0].Y < 3 + 10E-7, True)
            self.assertEqual(cNewPoints[0].appSegment, controlAppEdge)
            self.assertEqual(cNewPoints[0].paramCoord, intersecatore.resultParametricCoordinates[0, 0])
            self.assertEqual(cNewPoints[0].numVert, len(cNewPoints) - 1)
            self.assertEqual(cNewPoints[0].enumTypeIntersection, 1)

        except Exception as ex:
            self.fail()

    def test_OnlineIntersectionBegin(self):
        intersecatore = intersector.Intersector()
        A = Point(1.0, 0.0)
        B = Point(5.0, 0.0)
        C = Point(1.0, -6.0)
        D = Point(1.0, -2.0)
        A.numVert = 0
        cSegment = Segment(C, D)
        pSegment = Segment(A, B)
        cNewPoints = []
        controlAppEdge = 0
        controlInterPoints = []

        try:
            intersecatore.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, len(cNewPoints))
            self.assertEqual(pSegment.intersection, True)
            self.assertEqual(intersecatore.type, 1)
            self.assertEqual(1.5 - 10E-7 < intersecatore.resultParametricCoordinates[0, 0] < 1.5 + 10E-7, True)
            self.assertEqual(- 10E-7 < intersecatore.resultParametricCoordinates[1, 0] < 10E-7, True)
            self.assertEqual(len(cNewPoints), 0)
            self.assertEqual(len(controlInterPoints), 1)
            self.assertEqual(1 - 10E-7 < controlInterPoints[0].X < 1 + 10E-7, True)
            self.assertEqual(- 10E-7 < controlInterPoints[0].Y < 10E-7, True)
            self.assertEqual(controlInterPoints[0].appSegment, controlAppEdge)
            self.assertEqual(controlInterPoints[0].paramCoord, intersecatore.resultParametricCoordinates[0, 0])
            self.assertEqual(controlInterPoints[0].numVert, A.numVert)
            self.assertEqual(controlInterPoints[0].enumTypeIntersection, 0)

        except Exception as ex:
            self.fail()

    def test_OnSegmentIntersectionEnd(self):
        intersecatore = intersector.Intersector()
        A = Point(1.0, 0.0)
        B = Point(5.0, 0.0)
        C = Point(5.0, -6.0)
        D = Point(5.0, 6.0)
        B.numVert = 0
        cSegment = Segment(C, D)
        pSegment = Segment(A, B)
        cNewPoints = []
        controlAppEdge = 0
        controlInterPoints = []

        try:
            intersecatore.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, len(cNewPoints))
            self.assertEqual(pSegment.intersection, True)
            self.assertEqual(intersecatore.type, 2)
            self.assertEqual(0.5 - 10E-7 < intersecatore.resultParametricCoordinates[0, 0] < 0.5 + 10E-7, True)
            self.assertEqual(1 - 10E-7 < intersecatore.resultParametricCoordinates[1, 0] < 1 + 10E-7, True)
            self.assertEqual(len(cNewPoints), 0)
            self.assertEqual(len(controlInterPoints), 1)
            self.assertEqual(5 - 10E-7 < controlInterPoints[0].X < 5 + 10E-7, True)
            self.assertEqual(- 10E-7 < controlInterPoints[0].Y < 10E-7, True)
            self.assertEqual(controlInterPoints[0].appSegment, controlAppEdge)
            self.assertEqual(controlInterPoints[0].paramCoord, intersecatore.resultParametricCoordinates[0, 0])
            self.assertEqual(controlInterPoints[0].numVert, B.numVert)
            self.assertEqual(controlInterPoints[0].enumTypeIntersection, 2)

        except Exception as ex:
            self.fail()

    def test_NoIntersection(self):
        intersecatore = intersector.Intersector()
        A = Point(1.0, 0.0)
        B = Point(5.0, 0.0)
        C = Point(1.0, 1.0)
        D = Point(5.0, 1.0)
        cSegment = Segment(C, D)
        pSegment = Segment(A, B)
        cNewPoints = []
        controlAppEdge = 0
        controlInterPoints = []

        try:
            intersecatore.ComputeIntersection(cSegment, pSegment, cNewPoints, controlAppEdge, controlInterPoints, len(cNewPoints))
            self.assertEqual(pSegment.intersection, False)
            self.assertEqual(intersecatore.type, 0)

        except Exception as ex:
            self.fail()
